var express = require('express');
var router = express.Router();
var passport = require('passport');
var builder = require('xmlbuilder');
var errors = require('../api-errors');
var Server = require('../rumpus/server');
var server = new Server();
var _ = require('lodash');

/**
 * @api {get} /notices/xml/:noticeName Get Single Notice
 * @apiName GetNoticeXml
 * @apiGroup Notice-XML
 * @apiParam (URL Parameters) {String} noticeName Name of the upload notice
 * @apiQuery (Query Parameters) {String} apiKey The secret API key configured for the service
 * 
 * @apiExample {curl} Example usage:
 *  curl https://wadftp00.rohrer.com:3443/notices/xml/anotice?apikey=foobarbaz
 * 
 * @apiSuccess {Object} notice An XML structure with the following elements 
 * @apiSuccess {String} notice.noticeName 
 * @apiSuccess {String} notice.onePerSession
 * @apiSuccess {String} notice.mailServer
 * @apiSuccess {String} notice.mailFrom
 * @apiSuccess {String} notice.mailTo
 * @apiSuccess {String} notice.mailCC
 * @apiSuccess {String} notice.mailSubject
 * 
 * @apiSuccessExample {xml} Success Example:
 * 
 * <?xml version="1.0"?>
 * <notice>
 *    <noticeName>Insivia Uploads</noticeName>
 *    <onePerSession>Y</onePerSession>
 *    <mailServer>ns1.rohrer.com</mailServer>
 *    <mailFrom>rumpus@rohrer.com</mailFrom>
 *    <mailTo>corrigan@rohrer.com,matuscak@rohrer.com,ley@rohrer.com</mailTo>
 *    <mailCC/>
 *    <mailSubject>FTP File Drop Notification</mailSubject>
 * </notice>
 * 
 * @apiError Unauthorized Incorrect or missing api key
 * @apiError NotFound Notice not found
 */

router.get('/xml/:noticeName', passport.authenticate('localapikey', { session: false }), function(req, res, next) {
  server.findNotice(req.params['noticeName'], function(err, notice) {
    if (err)
      return next(err);

    if (notice)
      return res.type('xml').send(notice.toXml());

    return next(new errors.NotFoundError());
  });
});

/**
 * @api {get} /notices/xml Get All Notices with optional filter
 * @apiName GetNoticesXml
 * @apiGroup Notice-XML
 * @apiQuery (Query Parameters) {String} apiKey The secret API key configured for the service
 * @apiQuery (Query Parameters) {String} [noticeName] filter on notice name
 * @apiQuery (Query Parameters) {String} [onePerSession] filter on onePerSession flag (Y or N)
 * @apiQuery (Query Parameters) {String} [mailServer] filter on mail server
 * @apiQuery (Query Parameters) {String} [mailFrom] filter on from address
 * @apiQuery (Query Parameters) {String} [mailTo] filter on list of recipients
 * @apiQuery (Query Parameters) {String} [mailCC] filter on list of cc: recipients
 * @apiQuery (Query Parameters) {String} [mailSubject] filter on notice mail subject
 * 
 * @apiExample {curl} Example usage:
 *  curl https://wadftp00.rohrer.com:3443/notices/xml?apikey=foobarbaz&onePerSession=N
 * 
 * @apiSuccess {Object[]} notices An XML structure with the following elements
 * @apiSuccess {Object} notices.notice
 * @apiSuccess {String} notices.notice.noticeName 
 * @apiSuccess {String} notices.notice.onePerSession
 * @apiSuccess {String} notices.notice.mailServer
 * @apiSuccess {String} notices.notice.mailFrom
 * @apiSuccess {String} notices.notice.mailTo
 * @apiSuccess {String} notices.notice.mailCC
 * @apiSuccess {String} notices.notice.mailSubject
 * 
 * @apiSuccessExample {xml} Success Example:
 * <notices>
 *    <notice>
 *        <noticeName>Matuscak E-Mail</noticeName>
 *        <onePerSession>N</onePerSession>
 *        <mailServer>ns1.rohrer.com</mailServer>
 *        <mailFrom>rumpus@rohrer.com</mailFrom>
 *        <mailTo>matuscak@rohrer.com</mailTo>
 *        <mailCC/>
 *        <mailSubject>FTP File Notification</mailSubject>
 *    </notice>
 *    <notice>
 *        <noticeName>Password Lookup HTML</noticeName>
 *        <onePerSession>N</onePerSession>
 *        <mailServer>ns1.rohrer.com</mailServer>
 *        <mailFrom>helpdesk@rohrer.com</mailFrom>
 *        <mailTo/>
 *        <mailCC/>
 *        <mailSubject>Rohrer FTP Account Information</mailSubject>
 *    </notice>
 * </notices>
 * 
 * @apiError Unauthorized Incorrect or missing api key
 * @apiError NotFound Notice database empty
 */

router.get('/xml/', passport.authenticate('localapikey', { session: false }), function(req, res, next) {
  server.getNotices(function(err, notices) {
    if (err)
      return next(err);

    if (notices && notices.length > 0) {
      var xml = builder.create('notices');
      notices.forEach(function(notice) {
        xml.ele({notice: notice});
      });
      return res.type('xml').send(xml.end());
    }

    return next(new errors.NotFoundError());
  }, _.omit(req.query, ['apikey']));
});

/**
 * @api {get} /notices/:noticeName Get Single Notice
 * @apiName GetNotice
 * @apiGroup Notice-JSON
 * @apiParam (URL Parameters) {String} noticeName Name of the upload notice
 * @apiQuery (Query Parameters) {String} apiKey The secret API key configured for the service
 * 
 * @apiExample {curl} Example usage:
 *  curl https://wadftp00.rohrer.com:3443/notices/anotice?apikey=foobarbaz
 * 
 * @apiSuccess {Object} notice A JSON object with the following elements 
 * @apiSuccess {String} notice.noticeName 
 * @apiSuccess {String} notice.onePerSession
 * @apiSuccess {String} notice.mailServer
 * @apiSuccess {String} notice.mailFrom
 * @apiSuccess {String} notice.mailTo
 * @apiSuccess {String} notice.mailCC
 * @apiSuccess {String} notice.mailSubject
 * 
 * @apiSuccessExample {json} Success Example:
 {"noticeName":"Insivia Uploads","onePerSession":"Y","mailServer":"ns1.rohrer.com","mailFrom":"rumpus@rohrer.com","mailTo":"corrigan@rohrer.com,matuscak@rohrer.com,ley@rohrer.com","mailCC":"","mailSubject":"FTP File Drop Notification"}
 * 
 * @apiError Unauthorized Incorrect or missing api key
 * @apiError NotFound Notice not found
 */

router.get('/:noticeName', passport.authenticate('localapikey', { session: false }), function(req, res, next) {
  server.findNotice(req.params['noticeName'], function(err, notice) {
    if (err)
      return next(err);

    if (notice)
      return res.json(notice);

    return next(new errors.NotFoundError());
  });
});

/**
 * @api {get} /notices Get All Notices with optional filter
 * @apiName GetNotices
 * @apiGroup Notice-JSON
 * @apiQuery (Query Parameters) {String} apiKey The secret API key configured for the service
 * @apiQuery (Query Parameters) {String} [noticeName] filter on notice name
 * @apiQuery (Query Parameters) {String} [onePerSession] filter on onePerSession flag (Y or N)
 * @apiQuery (Query Parameters) {String} [mailServer] filter on mail server
 * @apiQuery (Query Parameters) {String} [mailFrom] filter on from address
 * @apiQuery (Query Parameters) {String} [mailTo] filter on list of recipients
 * @apiQuery (Query Parameters) {String} [mailCC] filter on list of cc: recipients
 * @apiQuery (Query Parameters) {String} [mailSubject] filter on notice mail subject
 * 
 * @apiExample {curl} Example usage:
 *  curl https://wadftp00.rohrer.com:3443/notices?apikey=foobarbaz&onePerSession=N
 * 
 * @apiSuccess {Object[]} notices A JSON array of notice objects
 * @apiSuccess {Object} notices.notice a notice object
 * @apiSuccess {String} notices.notice.noticeName 
 * @apiSuccess {String} notices.notice.onePerSession
 * @apiSuccess {String} notices.notice.mailServer
 * @apiSuccess {String} notices.notice.mailFrom
 * @apiSuccess {String} notices.notice.mailTo
 * @apiSuccess {String} notices.notice.mailCC
 * @apiSuccess {String} notices.notice.mailSubject
 * 
 * @apiSuccessExample {json} Success Example:
 * [
 *  {
 *    "noticeName":"Matuscak E-Mail",
 *    "onePerSession":"N",
 *    "mailServer":"ns1.rohrer.com",
 *    "mailFrom":"rumpus@rohrer.com",
 *    "mailTo":"matuscak@rohrer.com",
 *    "mailCC":"",
 *    "mailSubject":"FTP File Notification"
 *  },
 *  {
 *    "noticeName":"Password Lookup HTML",
 *    "onePerSession":"N",
 *    "mailServer":"ns1.rohrer.com",
 *    "mailFrom":"helpdesk@rohrer.com",
 *    "mailTo":"",
 *    "mailCC":"",
 *    "mailSubject":"Rohrer FTP Account Information"
 *  }
 * ]
 * 
 * @apiError Unauthorized Incorrect or missing api key
 * @apiError NotFound Notice database empty
 */

router.get('/', passport.authenticate('localapikey', { session: false }), function(req, res, next) {
  server.getNotices(function(err, notices) {
    if (err)
      return next(err);

    if (notices && notices.length > 0) 
      return res.json(notices);

    return next(new errors.NotFoundError());
  }, _.omit(req.query, ['apikey']));
});

module.exports = router;
