var express = require('express');
var router = express.Router();
var pjson = require('../package.json');

/* GET home page. */
// eslint-disable-next-line no-unused-vars
router.get('/', function(req, res, next) { 
  res.type('html');
  res.write('<h1>Service available</h1>');
  res.write('<p><a href="doc/api/index.html">API documentation</a></p>');
  res.write('<p><a href="doc/RumpusUserService/' + pjson.version + '/index.html">Source documentation</a></p>');
  res.end();
});

module.exports = router;
