var express = require('express');
var router = express.Router();
var passport = require('passport');
var User = require('../rumpus/user');
var builder = require('xmlbuilder');
var errors = require('../api-errors');
var Server = require('../rumpus/server');
var server = new Server();
var _ = require('lodash');

/**
 * @api {get} /users/xml/:userId Get Single User
 * @apiName GetUserXml
 * @apiGroup User-XML
 * @apiParam (URL Parameters) {String} userId  Name of the user account
 * @apiQuery (Query Parameters) {String} apiKey The secret API key configured for the service
 * 
 * @apiExample {curl} Example usage:
 *  curl https://wadftp00.rohrer.com:3443/users/xml/someuser?apikey=foobarbaz
 * 
 * @apiSuccess {Object} user An XML structure with the following elements (see <a href="#api-XML-PutUserXml">Modify Single User</a> for details)
 * @apiSuccess {String} user.accountName 
 * @apiSuccess {String} user.password
 * @apiSuccess {String} user.homeFolder
 * @apiSuccess {String} user.permissions
 * @apiSuccess {String} user.maxFolderSize
 * @apiSuccess {String} user.folderSetId
 * @apiSuccess {String} user.uploadNoticeName
 * @apiSuccess {String} user.maxSimultaneousConnections
 * @apiSuccess {String} user.maxUploadRate
 * @apiSuccess {String} user.maxUploadDownloadRatio
 * @apiSuccess {String} user.customFilePermissionSettings
 * @apiSuccess {String} user.accountExpirationInfo
 * @apiSuccess {String} user.maxDownloadRate
 * @apiSuccess {String} user.customOwner
 * @apiSuccess {String} user.downloadNoticeName
 * @apiSuccess {String} user.userEmailAddress
 * @apiSuccess {String} user.uploadCenter
 * @apiSuccess {String} user.alternateWFMAppearance
 * @apiSuccess {String} user.secondaryUploadNotice
 * @apiSuccess {String} user.customerNumber
 * 
 * @apiSuccessExample {xml} Success Example:
 * <?xml version="1.0"?>
 * <user>
 *    <accountName>rjcorrig</accountName>
 *    <password>mcrypt:-1588371341,1812714349,1022758108,-1199201178</password>
 *    <homeFolder>/Users/Shared/FTP Drops/rjcorrig/</homeFolder>
 *    <permissions>YYYYYYYYNNNYYYNN</permissions>
 *    <maxFolderSize>0</maxFolderSize>
 *    <folderSetId>0</folderSetId>
 *    <uploadNoticeName>Wadsworth Artwork</uploadNoticeName>
 *    <maxSimultaneousConnections>N4</maxSimultaneousConnections>
 *    <maxUploadRate>N16</maxUploadRate>
 *    <maxUploadDownloadRatio>N100</maxUploadDownloadRatio>
 *    <customFilePermissionSettings>NBRR</customFilePermissionSettings>
 *    <accountExpirationInfo>P</accountExpirationInfo>
 *    <maxDownloadRate>N16</maxDownloadRate>
 *    <customOwner>N-</customOwner>
 *    <downloadNoticeName/>
 *    <userEmailAddress/>
 *    <uploadCenter/>
 *    <alternateWFMAppearance>.</alternateWFMAppearance>
 *    <secondaryUploadNotice/>
 *    <customerNumber/>
 * </user>
 * 
 * @apiError Unauthorized Incorrect or missing api key
 * @apiError NotFound User not found
 */

router.get('/xml/:userId', passport.authenticate('localapikey', { session: false }), function(req, res, next) {
  server.findUser(req.params['userId'], function(err, user) {
    if (err)
      return next(err);

    if (user)
      return res.type('xml').send(user.toXml());

    return next(new errors.NotFoundError());
  });
});

/**
 * @api {get} /users/xml Get All Users with optional filter
 * @apiName GetUsersXml
 * @apiGroup User-XML
 * @apiQuery (Query Parameters) {String} apiKey The secret API key configured for the service
 * @apiQuery (Query Parameters) {String} [accountName] filter on login name
 * @apiQuery (Query Parameters) {String} [customerNumber] filter on customer number
 * @apiQuery (Query Parameters) {String} [password] filter on password
 * @apiQuery (Query Parameters) {String} [homeFolder] filter on user home folder
 * @apiQuery (Query Parameters) {String} [permissions] filter on permissions string
 * @apiQuery (Query Parameters) {String} [maxFolderSize] filter on maximum folder size
 * @apiQuery (Query Parameters) {String} [folderSetId] filter on a numeric ID from the Rumpus.fsets file
 * @apiQuery (Query Parameters) {String} [uploadNoticeName] filter on upload notice name
 * @apiQuery (Query Parameters) {String} [maxSimultaneousConnections] filter on max simultaneous connections
 * @apiQuery (Query Parameters) {String} [maxUploadRate] filter on maximum upload rate
 * @apiQuery (Query Parameters) {String} [maxUploadDownloadRatio] filter on maximum up/down ratio
 * @apiQuery (Query Parameters) {String} [customFilePermissionSettings] filter on file permissions
 * @apiQuery (Query Parameters) {String} [accountExpirationInfo] filter on expiration info
 * @apiQuery (Query Parameters) {String} [maxDownloadRate] filter on maximum download rate
 * @apiQuery (Query Parameters) {String} [customOwner] filter on custom owner
 * @apiQuery (Query Parameters) {String} [downloadNoticeName] filter on download notice name
 * @apiQuery (Query Parameters) {String} [userEmailAddress] filter on the e-mail address of the user
 * @apiQuery (Query Parameters) {String} [uploadCenter] filter on the name of an Upload Center form for use for this user
 * @apiQuery (Query Parameters) {String} [alternateWFMAppearance] filter on the domain name to be displayed
 * @apiQuery (Query Parameters) {String} [secondaryUploadNotice] filter on 2nd event notice for uploads
 * 
 * @apiExample {curl} Example usage:
 *  curl https://wadftp00.rohrer.com:3443/users/xml?apikey=foobarbaz&customerNumber=123456
 * 
 * @apiSuccess {Object[]} users An XML structure with the following elements (see <a href="#api-XML-PutUserXml">Modify Single User</a> for details)
 * @apiSuccess {Object} users.user
 * @apiSuccess {String} users.user.accountName 
 * @apiSuccess {String} users.user.password
 * @apiSuccess {String} users.user.homeFolder
 * @apiSuccess {String} users.user.permissions
 * @apiSuccess {String} users.user.maxFolderSize
 * @apiSuccess {String} users.user.folderSetId
 * @apiSuccess {String} users.user.uploadNoticeName
 * @apiSuccess {String} users.user.maxSimultaneousConnections
 * @apiSuccess {String} users.user.maxUploadRate
 * @apiSuccess {String} users.user.maxUploadDownloadRatio
 * @apiSuccess {String} users.user.customFilePermissionSettings
 * @apiSuccess {String} users.user.accountExpirationInfo
 * @apiSuccess {String} users.user.maxDownloadRate
 * @apiSuccess {String} users.user.customOwner
 * @apiSuccess {String} users.user.downloadNoticeName
 * @apiSuccess {String} users.user.userEmailAddress
 * @apiSuccess {String} users.user.uploadCenter
 * @apiSuccess {String} users.user.alternateWFMAppearance
 * @apiSuccess {String} users.user.secondaryUploadNotice
 * @apiSuccess {String} users.user.customerNumber
 * 
 * @apiSuccessExample {xml} Success Example:
 * <?xml version="1.0"?>
 * <users>
 *  <user>...</user>
 *  <user>...</user>
 *  <user>
 *    <accountName>rjcorrig</accountName>
 *    <password>mcrypt:-1588371341,1812714349,1022758108,-1199201178</password>
 *    <homeFolder>/Users/Shared/FTP Drops/rjcorrig/</homeFolder>
 *    <permissions>YYYYYYYYNNNYYYNN</permissions>
 *    <maxFolderSize>0</maxFolderSize>
 *    <folderSetId>0</folderSetId>
 *    <uploadNoticeName>Wadsworth Artwork</uploadNoticeName>
 *    <maxSimultaneousConnections>N4</maxSimultaneousConnections>
 *    <maxUploadRate>N16</maxUploadRate>
 *    <maxUploadDownloadRatio>N100</maxUploadDownloadRatio>
 *    <customFilePermissionSettings>NBRR</customFilePermissionSettings>
 *    <accountExpirationInfo>P</accountExpirationInfo>
 *    <maxDownloadRate>N16</maxDownloadRate>
 *    <customOwner>N-</customOwner>
 *    <downloadNoticeName/>
 *    <userEmailAddress/>
 *    <uploadCenter/>
 *    <alternateWFMAppearance>.</alternateWFMAppearance>
 *    <secondaryUploadNotice/>
 *    <customerNumber/>
 *  </user>
 *  <user>...</user>
 *  <user>...</user>
 * </users>
 * 
 * @apiError Unauthorized Incorrect or missing api key
 * @apiError NotFound User database empty
 */

router.get('/xml/', passport.authenticate('localapikey', { session: false }), function(req, res, next) {
  server.getUsers(function(err, users) {
    if (err)
      return next(err);

    if (users && users.length > 0) {
      var xml = builder.create('users');
      users.forEach(function(user) {
        xml.ele({user: user});
      });
      return res.type('xml').send(xml.end());
    }

    return next(new errors.NotFoundError());
  }, _.omit(req.query, ['apikey']));
});

/**
 * @api {put} /users/xml/:userId Modify Single User
 * @apiName PutUserXml
 * @apiGroup User-XML
 * @apiParam (URL Parameters) {String} userId  Name of the user account
 * @apiQuery (Query Parameters) {String} apiKey The secret API key configured for the service
 * @apiBody (Body Parameters) {Object} user An XML structure with the following elements
 * @apiBody (Body Parameters) {String} user.accountName login name
 * @apiBody (Body Parameters) {String} user.password user account password, encrypted or plain text
 * @apiBody (Body Parameters) {String} user.homeFolder ROOT or full path to user home folder
 * @apiBody (Body Parameters) {String} user.permissions The “Permissions” field is a single string of “Y/N” values representing the following permission values:<ul><li>Restrict To Home Folder</li><li>Permit Downloads</li><li>Permit Uploads</li><li>Permit Deletions</li><li>Permit Creation Of Folders</li><li>Permit Deletion Of Folders</li><li>Permit Login</li><li>Permit View Directories</li><li>See Invisible “Dot” Files (files with a name that begins with a period)</li><li>Administrator Privileges (allow user to manage user accounts)</li><li>Expire Files (delete files uploaded via Rumpus after specified time)</li>Permit FTP Access</li><li>Permit HTTP Access</li><li>Permit WebDAV Access</li><li>Permit Drop Shipping</li><li>Permit File Requests</li></ul>
 * @apiBody (Body Parameters) {String} user.maxFolderSize in MB
 * @apiBody (Body Parameters) {String} user.folderSetId a numeric ID from the Rumpus.fsets file
 * @apiBody (Body Parameters) {String} user.uploadNoticeName must excactly match a defined notice name
 * @apiBody (Body Parameters) {String} user.maxSimultaneousConnections Y or N to enable, followed by value
 * @apiBody (Body Parameters) {String} user.maxUploadRate Y or N to enable, followed by value in Kbps
 * @apiBody (Body Parameters) {String} user.maxUploadDownloadRatio Y or N to enable, followed by value
 * @apiBody (Body Parameters) {String} user.customFilePermissionSettings Y or N to enable, plus "N" none, "R" read or "B" read/write
 * @apiBody (Body Parameters) {String} user.accountExpirationInfo P - permanent, D - disable, R - remove
 * @apiBody (Body Parameters) {String} user.maxDownloadRate Y or N to enable, followed by value in Kbps
 * @apiBody (Body Parameters) {String} user.customOwner Y or N determines parent folder, plus account name
 * @apiBody (Body Parameters) {String} user.downloadNoticeName the name of an Upload Notice to be triggered for downloads
 * @apiBody (Body Parameters) {String} user.userEmailAddress the e-mail address of the user
 * @apiBody (Body Parameters) {String} user.uploadCenter the name of an Upload Center form for use for this user
 * @apiBody (Body Parameters) {String} user.alternateWFMAppearance the domain name to be displayed
 * @apiBody (Body Parameters) {String} user.secondaryUploadNotice a 2nd event notice may be triggered for uploads
 *
 * @apiExample {curl} Example usage:
 *  curl https://wadftp00.rohrer.com:3443/users/xml/someuser?apikey=foobarbaz
 * 
 * @apiSuccess (Success 201 Created) Created The user object was created
 * @apiSuccess (Success 204 No Content) NoContent The user object was updated
 * 
 * @apiError BadRequest User object failed validation, error in response text
 * @apiError Unauthorized Incorrect or missing api key
 * @apiError NotFound User not found
 */

router.put('/xml/:userId', passport.authenticate('localapikey', { session: false }), function(req, res, next) {

  if (!req.body.user)
    return next(new errors.BadRequestError('No user object was provided'));

  try {
    var user = new User(req.body.user, { complete: true });
  } catch (ex) {
    if (ex.message.includes('is required') ) {
      return next(new errors.BadRequestError(ex.message));
    }

    return next(ex);
  }

  if (user.accountName !== req.params['userId']) {
    return next(new errors.BadRequestError('accountName must match userId from URL'));
  }

  user.validate(function (err) {
    if (err)
      return next(new errors.BadRequestError(err));

    server.save(user, function(err, created) {
      if (err) 
        return next(err);

      server.reloadUserDatabase(function (err) {
        if (err) 
          return next(err);

        if (created) {
          return res.sendStatus(201);
        } else {
          return res.sendStatus(204);
        }
      });
    });
  });
});

router.put('/xml/', passport.authenticate('localapikey', { session: false }), function(req, res, next) {
  return next(new errors.NotFoundError());
});

/**
 * @api {delete} /users/xml/:userId Delete Single User
 * @apiName DeleteUserXml
 * @apiGroup User-XML
 * @apiParam (URL Parameters) {String} userId  Name of the user account
 * @apiQuery (Query Parameters) {String} apiKey The secret API key configured for the service
 * 
 * @apiExample {curl} Example usage:
 *  curl -X DELETE https://wadftp00.rohrer.com:3443/users/xml/someuser?apikey=foobarbaz
 * 
 * @apiSuccess (Success 204 No Content) NoContent The user object was deleted
 * 
 * @apiError Unauthorized Incorrect or missing api key
 * @apiError Forbidden User is protected from deletion
 * @apiError NotFound User not found
 */

router.delete('/xml/:userId', passport.authenticate('localapikey', { session: false }), function(req, res, next) {
  server.delete(req.params['userId'], function(err) {
    if (err) {
      if (err.message.includes('not found')) {
        return next(new errors.NotFoundError());
      } else if (err.message.includes('denied')) {
        return next(new errors.ForbiddenError());
      } else {
        return next(err);
      }
    }

    return res.sendStatus(204);
  });
});


/**
 * @api {get} /users/:userId Get Single User
 * @apiName GetUser
 * @apiGroup User-JSON
 * @apiParam (URL Parameters) {String} userId  Name of the user account
 * @apiQuery (Query Parameters) {String} apiKey The secret API key configured for the service
 * 
 * @apiExample {curl} Example usage:
 *  curl https://wadftp00.rohrer.com:3443/users/someuser?apikey=foobarbaz
 * 
 * @apiSuccess {Object} user A JSON object with the following elements (see <a href="#api-JSON-PutUser">Modify Single User</a> for details)
 * @apiSuccess {String} user.accountName 
 * @apiSuccess {String} user.password
 * @apiSuccess {String} user.homeFolder
 * @apiSuccess {String} user.permissions
 * @apiSuccess {String} user.maxFolderSize
 * @apiSuccess {String} user.folderSetId
 * @apiSuccess {String} user.uploadNoticeName
 * @apiSuccess {String} user.maxSimultaneousConnections
 * @apiSuccess {String} user.maxUploadRate
 * @apiSuccess {String} user.maxUploadDownloadRatio
 * @apiSuccess {String} user.customFilePermissionSettings
 * @apiSuccess {String} user.accountExpirationInfo
 * @apiSuccess {String} user.maxDownloadRate
 * @apiSuccess {String} user.customOwner
 * @apiSuccess {String} user.downloadNoticeName
 * @apiSuccess {String} user.userEmailAddress
 * @apiSuccess {String} user.uploadCenter
 * @apiSuccess {String} user.alternateWFMAppearance
 * @apiSuccess {String} user.secondaryUploadNotice
 * @apiSuccess {String} user.customerNumber
 * 
 * @apiSuccessExample {json} Success Example:
{"accountName":"rjcorrig","password":"mcrypt:-1588371341,1812714349,1022758108,-1199201178","homeFolder":"/Users/Shared/FTP Drops/rjcorrig/","permissions":"YYYYYYYYNNNYYYNN","maxFolderSize":"0","folderSetId":"0","uploadNoticeName":"Wadsworth Artwork","maxSimultaneousConnections":"N4","maxUploadRate":"N16","maxUploadDownloadRatio":"N100","customFilePermissionSettings":"NBRR","accountExpirationInfo":"P","maxDownloadRate":"N16","customOwner":"N-","downloadNoticeName":"","userEmailAddress":"","uploadCenter":"","alternateWFMAppearance":".","secondaryUploadNotice":"","customerNumber":""}
 * 
 * @apiError Unauthorized Incorrect or missing api key
 * @apiError NotFound User not found
 */


router.get('/:userId', passport.authenticate('localapikey', { session: false }), function(req, res, next) {
  server.findUser(req.params['userId'], function(err, user) {
    if (err)
      return next(err);

    if (user)
      return res.json(user);

    return next(new errors.NotFoundError());
  });
});

/**
 * @api {get} /users Get All Users with optional filter
 * @apiName GetUsers
 * @apiGroup User-JSON
 * @apiQuery (Query Parameters) {String} apiKey The secret API key configured for the service
 * @apiQuery (Query Parameters) {String} [customerNumber] filter on customer number 
 * @apiQuery (Query Parameters) {String} [accountName] filter on login name
 * @apiQuery (Query Parameters) {String} [password] filter on password
 * @apiQuery (Query Parameters) {String} [homeFolder] filter on user home folder
 * @apiQuery (Query Parameters) {String} [permissions] filter on permissions string
 * @apiQuery (Query Parameters) {String} [maxFolderSize] filter on maximum folder size
 * @apiQuery (Query Parameters) {String} [folderSetId] filter on a numeric ID from the Rumpus.fsets file
 * @apiQuery (Query Parameters) {String} [uploadNoticeName] filter on upload notice name
 * @apiQuery (Query Parameters) {String} [maxSimultaneousConnections] filter on max simultaneous connections
 * @apiQuery (Query Parameters) {String} [maxUploadRate] filter on maximum upload rate
 * @apiQuery (Query Parameters) {String} [maxUploadDownloadRatio] filter on maximum up/down ratio
 * @apiQuery (Query Parameters) {String} [customFilePermissionSettings] filter on file permissions
 * @apiQuery (Query Parameters) {String} [accountExpirationInfo] filter on expiration info
 * @apiQuery (Query Parameters) {String} [maxDownloadRate] filter on maximum download rate
 * @apiQuery (Query Parameters) {String} [customOwner] filter on custom owner
 * @apiQuery (Query Parameters) {String} [downloadNoticeName] filter on download notice name
 * @apiQuery (Query Parameters) {String} [userEmailAddress] filter on the e-mail address of the user
 * @apiQuery (Query Parameters) {String} [uploadCenter] filter on the name of an Upload Center form for use for this user
 * @apiQuery (Query Parameters) {String} [alternateWFMAppearance] filter on the domain name to be displayed
 * @apiQuery (Query Parameters) {String} [secondaryUploadNotice] filter on 2nd event notice for uploads
 * 
 * @apiExample {curl} Example usage:
 *  curl https://wadftp00.rohrer.com:3443/users?apikey=foobarbaz&customerNumber=123456
 * 
 * @apiSuccess {Object[]} users An JSON array of user objects
 * @apiSuccess {Object} users.user a user object (see <a href="#api-JSON-PutUser">Modify Single User</a> for details)
 * @apiSuccess {String} users.user.accountName 
 * @apiSuccess {String} users.user.password
 * @apiSuccess {String} users.user.homeFolder
 * @apiSuccess {String} users.user.permissions
 * @apiSuccess {String} users.user.maxFolderSize
 * @apiSuccess {String} users.user.folderSetId
 * @apiSuccess {String} users.user.uploadNoticeName
 * @apiSuccess {String} users.user.maxSimultaneousConnections
 * @apiSuccess {String} users.user.maxUploadRate
 * @apiSuccess {String} users.user.maxUploadDownloadRatio
 * @apiSuccess {String} users.user.customFilePermissionSettings
 * @apiSuccess {String} users.user.accountExpirationInfo
 * @apiSuccess {String} users.user.maxDownloadRate
 * @apiSuccess {String} users.user.customOwner
 * @apiSuccess {String} users.user.downloadNoticeName
 * @apiSuccess {String} users.user.userEmailAddress
 * @apiSuccess {String} users.user.uploadCenter
 * @apiSuccess {String} users.user.alternateWFMAppearance
 * @apiSuccess {String} users.user.secondaryUploadNotice
 * @apiSuccess {String} users.user.customerNumber
 * 
 * @apiSuccessExample {json} Success Example: 
 * [
 *  {
 *    "accountName": "foobar",
 *    "password":"mcrypt:1902954184,1896784868,1040073348,650670142",
 *    "homeFolder":"/Users/Shared/FTP Drops/foobar/",
 *    "permissions":"YYYYYYYYNNNYYYNN",
 *    "maxFolderSize":"0",
 *    "folderSetId":"0",
 *    "uploadNoticeName":"Wadsworth Artwork",
 *    "maxSimultaneousConnections":"N4",
 *    "maxUploadRate":"N16",
 *    "maxUploadDownloadRatio":"N100",
 *    "customFilePermissionSettings":"NBRR",
 *    "accountExpirationInfo":"P",
 *    "maxDownloadRate":"N16",
 *    "customOwner":"N-",
 *    "downloadNoticeName":"",
 *    "userEmailAddress":"",
 *    "uploadCenter":"",
 *    "alternateWFMAppearance":".",
 *    "secondaryUploadNotice":"",
 *    "customerNumber":""
 *  },
 *  {
 *    "accountName":"rjcorrig",
 *    "password":"mcrypt:-1588371341,1812714349,1022758108,-1199201178",
 *    "homeFolder":"/Users/Shared/FTP Drops/rjcorrig/",
 *    "permissions":"YYYYYYYYNNNYYYNN",
 *    "maxFolderSize":"0",
 *    "folderSetId":"0",
 *    "uploadNoticeName":"Wadsworth Artwork",
 *    "maxSimultaneousConnections":"N4",
 *    "maxUploadRate":"N16",
 *    "maxUploadDownloadRatio":"N100",
 *    "customFilePermissionSettings":"NBRR",
 *    "accountExpirationInfo":"P",
 *    "maxDownloadRate":"N16",
 *    "customOwner":"N-",
 *    "downloadNoticeName":"",
 *    "userEmailAddress":"",
 *    "uploadCenter":"",
 *    "alternateWFMAppearance":".",
 *    "secondaryUploadNotice":"","customerNumber:""
 *  }
 * ]
 * 
 * @apiError Unauthorized Incorrect or missing api key
 * @apiError NotFound User database empty
 */

router.get('/', passport.authenticate('localapikey', { session: false }), function(req, res, next) {
  server.getUsers(function(err, users) {
    if (err)
      return next(err);

    if (users && users.length > 0) 
      return res.json(users);

    return next(new errors.NotFoundError());
  }, _.omit(req.query, ['apikey']));
});

/**
 * @api {put} /users/:userId Modify Single User
 * @apiName PutUser
 * @apiGroup User-JSON
 * @apiParam (URL Parameters) {String} userId  Name of the user account
 * @apiQuery (Query Parameters) {String} apiKey The secret API key configured for the service
 * @apiBody (Body Parameters) {Object} user An unnamed JSON object with the following elements
 * @apiBody (Body Parameters) {String} user.accountName login name
 * @apiBody (Body Parameters) {String} user.password user account password, encrypted or plain text
 * @apiBody (Body Parameters) {String} user.homeFolder ROOT or full path to user home folder
 * @apiBody (Body Parameters) {String} user.permissions The “Permissions” field is a single string of “Y/N” values representing the following permission values:<ul><li>Restrict To Home Folder</li><li>Permit Downloads</li><li>Permit Uploads</li><li>Permit Deletions</li><li>Permit Creation Of Folders</li><li>Permit Deletion Of Folders</li><li>Permit Login</li><li>Permit View Directories</li><li>See Invisible “Dot” Files (files with a name that begins with a period)</li><li>Administrator Privileges (allow user to manage user accounts)</li><li>Expire Files (delete files uploaded via Rumpus after specified time)</li>Permit FTP Access</li><li>Permit HTTP Access</li><li>Permit WebDAV Access</li><li>Permit Drop Shipping</li><li>Permit File Requests</li></ul>
 * @apiBody (Body Parameters) {String} user.maxFolderSize in MB
 * @apiBody (Body Parameters) {String} user.folderSetId a numeric ID from the Rumpus.fsets file
 * @apiBody (Body Parameters) {String} user.uploadNoticeName must exactly match a defined notice name
 * @apiBody (Body Parameters) {String} user.maxSimultaneousConnections Y or N to enable, followed by value
 * @apiBody (Body Parameters) {String} user.maxUploadRate Y or N to enable, followed by value in Kbps
 * @apiBody (Body Parameters) {String} user.maxUploadDownloadRatio Y or N to enable, followed by value
 * @apiBody (Body Parameters) {String} user.customFilePermissionSettings Y or N to enable, plus "N" none, "R" read or "B" read/write
 * @apiBody (Body Parameters) {String} user.accountExpirationInfo P - permanent, D - disable, R - remove
 * @apiBody (Body Parameters) {String} user.maxDownloadRate Y or N to enable, followed by value in Kbps
 * @apiBody (Body Parameters) {String} user.customOwner Y or N determines parent folder, plus account name
 * @apiBody (Body Parameters) {String} user.downloadNoticeName the name of an Upload Notice to be triggered for downloads
 * @apiBody (Body Parameters) {String} user.userEmailAddress the e-mail address of the user
 * @apiBody (Body Parameters) {String} user.uploadCenter the name of an Upload Center form for use for this user
 * @apiBody (Body Parameters) {String} user.alternateWFMAppearance the domain name to be displayed
 * @apiBody (Body Parameters) {String} user.secondaryUploadNotice a 2nd event notice may be triggered for uploads
 * 
 * @apiExample {curl} Example usage:
 *  curl https://wadftp00.rohrer.com:3443/users/someuser?apikey=foobarbaz
 * 
 * @apiSuccess (Success 201 Created) Created The user object was created
 * @apiSuccess (Success 204 No Content) NoContent The user object was updated
 * 
 * @apiError BadRequest User object failed validation, error in response text
 * @apiError Unauthorized Incorrect or missing api key
 * @apiError NotFound User not found
 */

router.put('/:userId', passport.authenticate('localapikey', { session: false }), function(req, res, next) {

  try {
    var user = new User(req.body, { complete: true });
  } catch (ex) {
    if (ex.message.includes('is required') ) {
      return next(new errors.BadRequestError(ex.message));
    }

    return next(ex);
  }
  
  if (user.accountName !== req.params['userId']) {
    return next(new errors.BadRequestError('accountName must match userId from URL'));
  }

  user.validate(function (err) {
    if (err)
      return next(new errors.BadRequestError(err));

    server.save(user, function(err, created) {
      if (err) 
        return next(err);

      server.reloadUserDatabase(function (err) {
        if (err) 
          return next(err);

        if (created) {
          return res.sendStatus(201);
        } else {
          return res.sendStatus(204);
        }
      });
    });
  });
});

/**
 * @api {delete} /users/:userId Delete Single User
 * @apiName DeleteUser
 * @apiGroup User-JSON
 * @apiParam (URL Parameters) {String} userId  Name of the user account
 * @apiQuery (Query Parameters) {String} apiKey The secret API key configured for the service
 * 
 * @apiExample {curl} Example usage:
 *  curl -X DELETE https://wadftp00.rohrer.com:3443/users/someuser?apikey=foobarbaz
 * 
 * @apiSuccess (Success 204 No Content) NoContent The user object was deleted
 * 
 * @apiError Unauthorized Incorrect or missing api key
 * @apiError Forbidden User is protected from deletion
 * @apiError NotFound User not found
 */

router.delete('/:userId', passport.authenticate('localapikey', { session: false }), function(req, res, next) {
  server.delete(req.params['userId'], function(err) {
    if (err) {
      if (err.message.includes('not found')) {
        return next(new errors.NotFoundError());
      } else if (err.message.includes('denied')) {
        return next(new errors.ForbiddenError());
      } else {
        return next(err);
      }
    } 

    return res.sendStatus(204);
  });
});

module.exports = router;
