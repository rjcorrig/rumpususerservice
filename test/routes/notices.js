var expect = require('chai').expect;
var request = require('supertest');
var app = require('../../app');
var config = require('config');
var fs = require('fs');
var path = require('path');
var Notice = require('../../rumpus/notice');
var parseString = require('xml2js').parseString;

var initApp = function() {
  if (process.env.NODE_ENV === 'production')
    throw new Error('Tests must only run in development mode');

  server = request.agent(app);

  // Reload original test file
  var testNoticeDb = path.join(__dirname, '../rumpus/Rumpus.notices.test');
  var noticeDb = fs.readFileSync(testNoticeDb);

  // Create test config path if it doesn't exist
  var configPath = config.get('Rumpus.configPath');
  if (!fs.existsSync(configPath))
    fs.mkdirSync(configPath);

  fs.writeFileSync(path.join(configPath, 'Rumpus.notices'), noticeDb);
};

describe('/api', function () {

  this.timeout(5000);
  beforeEach(initApp);

  describe('GET /notices', function() {
    
    it('should reject missing api key', function(done) {
      server.get('/notices')
        .send()
        .expect(401)
        .end(done);
    });

    it('should reject bad api key', function(done) {
      server.get('/notices?apikey=nosuchkey')
        .send()
        .expect(401)
        .end(done);
    });

    it('should accept proper api key', function(done) {
      server.get('/notices?apikey=' + config.get('Service.apiKey'))
        .send()
        .expect(200)
        .end(done);
    });

    it('should return 404 if no notice found', function(done) {
      server.get('/notices/nosuchnotice?apikey=' + config.get('Service.apiKey'))
        .send()
        .expect(404)
        .end(done);
    });

    it('should return 200 with body if notice found', function(done) {
      var noticeName = 'Administrator';
      server.get('/notices/' + noticeName + '?apikey=' + config.get('Service.apiKey'))
        .send()
        .expect(200)
        .end(function(err, res) {
          if (err) return done(err);

          expect(res.body).to.be.an('Object');
          expect(res.body.noticeName.toLowerCase()).to.equal(noticeName.toLowerCase());

          done();
        });
    });

    it('should return 200 with all notices if no notice requested', function(done) {
      server.get('/notices/?apikey=' + config.get('Service.apiKey'))
        .send()
        .expect(200)
        .end(function(err, res) {
          if (err) return done(err);

          expect(res.body).to.be.an('Array');
          expect(res.body[0]).to.have.property('noticeName');
          expect(res.body[res.body.length-1].noticeName).to.be.equal('Wadsworth Artwork');

          done();
        });
    });

    it('should return 200 with matching notices if a filter is passed', function(done) {
      var onePerSession = 'N';
      server.get('/notices?onePerSession=' + onePerSession + '&apikey=' + config.get('Service.apiKey'))
        .send()
        .expect(200)
        .end(function(err, res) {
          if (err) return done(err);

          expect(res.body).to.be.an('Array');
          expect(res.body.length).to.equal(2);
          expect(res.body[0]).to.have.property('onePerSession');
          expect(res.body[0].onePerSession).to.be.equal(onePerSession);
          expect(res.body[res.body.length-1].onePerSession).to.be.equal(onePerSession);

          done();
        });
    });

    it('should return 404 if no matching notices when a filter is passed', function(done) {
      var onePerSession = 'xyzzy';
      server.get('/notices?onePerSession=' + onePerSession + '&apikey=' + config.get('Service.apiKey'))
        .send()
        .expect(404)
        .end(done);
    });

  });

  describe('GET /notices/xml', function() {
    
    it('should reject missing api key', function(done) {
      server.get('/notices/xml')
        .send()
        .expect(401)
        .end(done);
    });

    it('should reject bad api key', function(done) {
      server.get('/notices/xml?apikey=nosuchkey')
        .send()
        .expect(401)
        .end(done);
    });

    it('should accept proper api key', function(done) {
      server.get('/notices/xml?apikey=' + config.get('Service.apiKey'))
        .send()
        .expect(200)
        .end(done);
    });

    it('should return 404 if no notice found', function(done) {
      server.get('/notices/xml/nosuchnotice?apikey=' + config.get('Service.apiKey'))
        .send()
        .expect(404)
        .end(done);
    });

    it('should return 200 with XML body if notice found', function(done) {
      var noticeName = 'Administrator';
      server.get('/notices/xml/' + noticeName + '?apikey=' + config.get('Service.apiKey'))
        .send()
        .expect('Content-Type', /xml/)
        .expect(200)
        .end(function(err, res) {
          if (err) return done(err);

          Notice.fromXml(res.text, function(err, notice) {
            if (err) return done(err);

            expect(notice.noticeName.toLowerCase()).to.equal(noticeName.toLowerCase());
            done();
          });
        });
    });

    it('should return 200 with all notices if no notice requested', function(done) {
      server.get('/notices/xml/?apikey=' + config.get('Service.apiKey'))
        .send()
        .expect('Content-Type', /xml/)
        .expect(200)
        .end(function(err, res) {
          if (err) return done(err);

          parseString(res.text, { explicitRoot: false }, function(err, notices) {
            if (err) return done(err);

            expect(notices.notice[0]).to.have.property('noticeName');
            expect(notices.notice[notices.notice.length-1].noticeName[0]).to.be.equal('Wadsworth Artwork');

            done();
          });
        });
    });

    it('should return 200 with matching notices if a filter is passed', function(done) {
      var onePerSession = 'N';
      server.get('/notices/xml?onePerSession=' + onePerSession + '&apikey=' + config.get('Service.apiKey'))
        .send()
        .expect('Content-Type', /xml/)
        .expect(200)
        .end(function(err, res) {
          if (err) return done(err);

          parseString(res.text, { explicitRoot: false }, function(err, notices) {
            if (err) return done(err);

            expect(notices.notice.length).to.equal(2);
            expect(notices.notice[0]).to.have.property('onePerSession');
            expect(notices.notice[0].onePerSession[0]).to.be.equal(onePerSession);
            expect(notices.notice[notices.notice.length-1].onePerSession[0]).to.be.equal(onePerSession);

            done();
          });
        });
    });

    it('should return 404 if no matching notices when a filter is passed', function(done) {
      var onePerSession = 'xyzzy';
      server.get('/notices/xml?uploadNoticeName=' + onePerSession + '&apikey=' + config.get('Service.apiKey'))
        .send()
        .expect(404)
        .end(done);
    });

  });


});
