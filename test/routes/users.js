var expect = require('chai').expect;
var request = require('supertest');
var app = require('../../app');
var config = require('config');
var fs = require('fs');
var path = require('path');
var User = require('../../rumpus/user');
var parseString = require('xml2js').parseString;

var initApp = function() {
  if (process.env.NODE_ENV === 'production')
    throw new Error('Tests must only run in development mode');

  server = request.agent(app);

  // Reload original test file
  var testUserDb = path.join(__dirname, '../rumpus/Rumpus.users.test');
  var userDb = fs.readFileSync(testUserDb);

  // Create test config path if it doesn't exist
  var configPath = config.get('Rumpus.configPath');
  if (!fs.existsSync(configPath))
    fs.mkdirSync(configPath);

  fs.writeFileSync(path.join(configPath, 'Rumpus.users'), userDb);
};

describe('/api', function() {
  this.timeout(5000);
  beforeEach(initApp);

  describe('GET /users', function() {
    
    it('should reject missing api key', function(done) {
      server.get('/users')
        .send()
        .expect(401)
        .end(done);
    });

    it('should reject bad api key', function(done) {
      server.get('/users?apikey=nosuchkey')
        .send()
        .expect(401)
        .end(done);
    });

    it('should accept proper api key', function(done) {
      server.get('/users?apikey=' + config.get('Service.apiKey'))
        .send()
        .expect(200)
        .end(done);
    });

    it('should return 404 if no user found', function(done) {
      server.get('/users/nosuchuser?apikey=' + config.get('Service.apiKey'))
        .send()
        .expect(404)
        .end(done);
    });

    it('should return 200 with body if user found', function(done) {
      var accountName = 'anonymous';
      server.get('/users/' + accountName + '?apikey=' + config.get('Service.apiKey'))
        .send()
        .expect(200)
        .end(function(err, res) {
          if (err) return done(err);

          expect(res.body).to.be.an('Object');
          expect(res.body.accountName.toLowerCase()).to.equal(accountName.toLowerCase());

          done();
        });
    });

    it('should return 200 with all users if no user requested', function(done) {
      server.get('/users/?apikey=' + config.get('Service.apiKey'))
        .send()
        .expect(200)
        .end(function(err, res) {
          if (err) return done(err);

          expect(res.body).to.be.an('Array');
          expect(res.body[0]).to.have.property('accountName');
          expect(res.body[res.body.length-1].accountName).to.be.equal('zobelepg');

          done();
        });
    });

    it('should return 200 with matching users if a filter is passed', function(done) {
      var uploadNoticeName = 'Administrator';
      server.get('/users?uploadNoticeName=' + uploadNoticeName + '&apikey=' + config.get('Service.apiKey'))
        .send()
        .expect(200)
        .end(function(err, res) {
          if (err) return done(err);

          expect(res.body).to.be.an('Array');
          expect(res.body.length).to.equal(28);
          expect(res.body[0]).to.have.property('uploadNoticeName');
          expect(res.body[0].uploadNoticeName).to.be.equal(uploadNoticeName);
          expect(res.body[res.body.length-1].uploadNoticeName).to.be.equal(uploadNoticeName);

          done();
        });
    });

    it('should return 404 if no matching users when a filter is passed', function(done) {
      var uploadNoticeName = 'xyzzy';
      server.get('/users?uploadNoticeName=' + uploadNoticeName + '&apikey=' + config.get('Service.apiKey'))
        .send()
        .expect(404)
        .end(done);
    });

  });

  describe('PUT /users', function() {
    this.timeout(5000);

    it('should return 404 if no user given', function(done) {
      server.put('/users')
        .send()
        .expect(404)
        .end(done);
    });

    it('should reject missing api key', function(done) {
      server.put('/users/nosuchuser')
        .send()
        .expect(401)
        .end(done);
    });

    it('should reject bad api key', function(done) {
      server.put('/users/nosuchuser?apikey=nosuchkey')
        .send()
        .expect(401)
        .end(done);
    });

    it('should reject empty body', function(done) {
      server.put('/users/nosuchuser?apikey=' + config.get('Service.apiKey'))
        .send()
        .expect(400)
        .end(done);
    });

    it('should reject incomplete body', function(done) {
      var accountName = 'newuser';
      var sentUser = new User({
        accountName: 'somethingElse',
        password: 'newpass'
      });

      delete sentUser.uploadNoticeName;

      server.put('/users/' + accountName + '?apikey=' + config.get('Service.apiKey'))
        .send(sentUser)
        .expect(400)
        .end(done);
    });

    it('should reject userId and accountName mismatch', function(done) {

      var accountName = 'newuser';
      var sentUser = new User({
        accountName: 'somethingElse',
        password: 'newpass'
      });

      server.put('/users/' + accountName + '?apikey=' + config.get('Service.apiKey'))
        .send(sentUser)
        .expect(400)
        .end(done);
    });

    it('should return 400 on invalid user', function(done) {

      var accountName = 'baduser';
      var sentUser = new User({
        accountName: accountName,
        homeFolder: '/'
      });

      server.put('/users/' + accountName + '?apikey=' + config.get('Service.apiKey'))
        .send(sentUser)
        .expect(400)
        .end(done);
    });

    it('should create a new user and return 201', function(done) {

      var accountName = 'newuser';
      var sentUser = new User({
        accountName: accountName,
        password: 'newpass'
      });

      server.put('/users/' + accountName + '?apikey=' + config.get('Service.apiKey'))
        .send(sentUser)
        .expect(201)
        .end(function(err, res) {
          if (err) return done(err);

          // Retrieve the user again and verify its password was changed
          server.get('/users/' + accountName + '?apikey=' + config.get('Service.apiKey'))
            .send()
            .expect(200)
            .end(function(err, res) {
              if (err) return done(err);

              recdUser = res.body;
              expect(recdUser).to.be.an('Object');
              expect(recdUser.accountName.toLowerCase()).to.equal(accountName.toLowerCase());
              expect(recdUser.password).to.equal(sentUser.password);

              done();
            });
        });

    });

    it('should update an existing user and return 204', function(done) {

      var accountName = '3mcommand';
      server.get('/users/' + accountName + '?apikey=' + config.get('Service.apiKey'))
        .send()
        .expect(200)
        .end(function(err, res) {
          if (err) return done(err);

          var sentUser = res.body;
          expect(sentUser).to.be.an('Object');
          expect(sentUser.accountName.toLowerCase()).to.equal(accountName.toLowerCase());
          sentUser.password = 'newpass';
          sentUser.homeFolder = path.join(config.get('Rumpus.ftpRoot'), sentUser.accountName);
          
          server.put('/users/' + accountName + '?apikey=' + config.get('Service.apiKey'))
            .send(sentUser)
            .expect(204)
            .end(function(err, res) {
              if (err) return done(err);

              // Retrieve the user again and verify its password was changed
              server.get('/users/' + accountName + '?apikey=' + config.get('Service.apiKey'))
                .send()
                .expect(200)
                .end(function(err, res) {
                  if (err) return done(err);

                  recdUser = res.body;
                  expect(recdUser).to.be.an('Object');
                  expect(recdUser.accountName.toLowerCase()).to.equal(accountName.toLowerCase());
                  expect(recdUser.password).to.equal(sentUser.password);

                  done();
                });
            });
        });
    });
  });

  describe('DELETE /users', function () {
    it('should return 404 if no user given', function(done) {
      server.delete('/users')
        .send()
        .expect(404)
        .end(done);
    });

    it('should reject missing api key', function(done) {
      server.delete('/users/nosuchuser')
        .send()
        .expect(401)
        .end(done);
    });

    it('should reject bad api key', function(done) {
      server.delete('/users/nosuchuser?apikey=nosuchkey')
        .send()
        .expect(401)
        .end(done);
    });

    it('should return 404 if user not found', function(done) {
      server.delete('/users/nosuchuser?apikey=' + config.get('Service.apiKey'))
        .send()
        .expect(404)
        .end(done);
    });

    it('should return 403 if user is protected', function(done) {
      var protected = config.get('ProtectedAccounts');

      server.delete('/users/' + protected[0] + '?apikey=' + config.get('Service.apiKey'))
        .send()
        .expect(403)
        .end(done);
    });

    it('should delete an existing user and return 204', function(done) {

      var accountName = '3mcommand';
      server.get('/users/' + accountName + '?apikey=' + config.get('Service.apiKey'))
        .send()
        .expect(200)
        .end(function(err, res) {
          if (err) return done(err);

          var sentUser = res.body;
          expect(sentUser).to.be.an('Object');
          expect(sentUser.accountName.toLowerCase()).to.equal(accountName.toLowerCase());

          server.delete('/users/' + accountName + '?apikey=' + config.get('Service.apiKey'))
            .send()
            .expect(204)
            .end(function(err, res) {
              if (err) return done(err);

              // Retrieve the user again and verify it's gone
              server.get('/users/' + accountName + '?apikey=' + config.get('Service.apiKey'))
                .send()
                .expect(404)
                .end(done);
            });
        });
    });

  });

  describe('GET /users/xml', function() {
    
    it('should reject missing api key', function(done) {
      server.get('/users/xml')
        .send()
        .expect(401)
        .end(done);
    });

    it('should reject bad api key', function(done) {
      server.get('/users/xml?apikey=nosuchkey')
        .send()
        .expect(401)
        .end(done);
    });

    it('should accept proper api key', function(done) {
      server.get('/users/xml?apikey=' + config.get('Service.apiKey'))
        .send()
        .expect(200)
        .end(done);
    });

    it('should return 404 if no user found', function(done) {
      server.get('/users/xml/nosuchuser?apikey=' + config.get('Service.apiKey'))
        .send()
        .expect(404)
        .end(done);
    });

    it('should return 200 with XML body if user found', function(done) {
      var accountName = 'anonymous';
      server.get('/users/xml/' + accountName + '?apikey=' + config.get('Service.apiKey'))
        .send()
        .expect('Content-Type', /xml/)
        .expect(200)
        .end(function(err, res) {
          if (err) return done(err);

          User.fromXml(res.text, function(err, user) {
            if (err) return done(err);

            expect(user.accountName.toLowerCase()).to.equal(accountName.toLowerCase());
            done();
          });
        });
    });

    it('should return 200 with all users if no user requested', function(done) {
      server.get('/users/xml/?apikey=' + config.get('Service.apiKey'))
        .send()
        .expect('Content-Type', /xml/)
        .expect(200)
        .end(function(err, res) {
          if (err) return done(err);

          parseString(res.text, { explicitRoot: false }, function(err, users) {
            if (err) return done(err);

            expect(users.user[0]).to.have.property('accountName');
            expect(users.user[users.user.length-1].accountName[0]).to.be.equal('zobelepg');

            done();
          });
        });
    });

    it('should return 200 with matching users if a filter is passed', function(done) {
      var uploadNoticeName = 'Administrator';
      server.get('/users/xml?uploadNoticeName=' + uploadNoticeName + '&apikey=' + config.get('Service.apiKey'))
        .send()
        .expect('Content-Type', /xml/)
        .expect(200)
        .end(function(err, res) {
          if (err) return done(err);

          parseString(res.text, { explicitRoot: false }, function(err, users) {
            if (err) return done(err);

            expect(users.user.length).to.equal(28);
            expect(users.user[0]).to.have.property('uploadNoticeName');
            expect(users.user[0].uploadNoticeName[0]).to.be.equal(uploadNoticeName);
            expect(users.user[users.user.length-1].uploadNoticeName[0]).to.be.equal(uploadNoticeName);

            done();
          });
        });
    });

    it('should return 404 if no matching users when a filter is passed', function(done) {
      var uploadNoticeName = 'xyzzy';
      server.get('/users/xml?uploadNoticeName=' + uploadNoticeName + '&apikey=' + config.get('Service.apiKey'))
        .send()
        .expect(404)
        .end(done);
    });

  });

  describe('PUT /users/xml', function() {
    it('should return 404 if no user given', function(done) {
      server.put('/users/xml?apikey=' + config.get('Service.apiKey'))
        .send()
        .expect(404)
        .end(done);
    });

    it('should reject missing api key', function(done) {
      server.put('/users/xml/nosuchuser')
        .send()
        .expect(401)
        .end(done);
    });

    it('should reject bad api key', function(done) {
      server.put('/users/xml/nosuchuser?apikey=nosuchkey')
        .send()
        .expect(401)
        .end(done);
    });

    it('should reject empty body', function(done) {
      server.put('/users/xml/nosuchuser?apikey=' + config.get('Service.apiKey'))
        .send()
        .expect(400)
        .end(done);
    });

    it('should reject incomplete body', function(done) {

      var accountName = 'newuser';
      var sentUser = new User({
        accountName: 'newuser',
        password: 'newpass'
      });

      delete sentUser.uploadNoticeName;

      server.put('/users/xml/' + accountName + '?apikey=' + config.get('Service.apiKey'))
        .set('Content-Type', 'application/xml')
        .send(sentUser.toXml())
        .expect(400)
        .end(done);
    });

    it('should reject userId and accountName mismatch', function(done) {

      var accountName = 'newuser';
      var sentUser = new User({
        accountName: 'somethingElse',
        password: 'newpass'
      });

      server.put('/users/xml/' + accountName + '?apikey=' + config.get('Service.apiKey'))
        .set('Content-Type', 'application/xml')
        .send(sentUser.toXml())
        .expect(400)
        .end(done);
    });

    it('should return 400 on invalid user', function(done) {
      var accountName = 'baduser';
      var sentUser = new User({
        accountName: accountName,
        homeFolder: '/'
      });

      server.put('/users/xml/' + accountName + '?apikey=' + config.get('Service.apiKey'))
        .set('Content-Type', 'application/xml')
        .send(sentUser.toXml())
        .expect(400)
        .end(done);
    });

    it('should create a new user and return 201', function(done) {

      var accountName = 'newuser';
      var sentUser = new User({
        accountName: accountName,
        password: 'newpass'
      });

      server.put('/users/xml/' + accountName + '?apikey=' + config.get('Service.apiKey'))
        .set('Content-Type', 'application/xml')
        .send(sentUser.toXml())
        .expect(201)
        .end(function(err, res) {
          if (err) return done(err);

          // Retrieve the user again and verify its password was changed
          server.get('/users/xml/' + accountName + '?apikey=' + config.get('Service.apiKey'))
            .send()
            .expect('Content-Type', /xml/)
            .expect(200)
            .end(function(err, res) {
              if (err) return done(err);

              User.fromXml(res.text, function(err, recdUser) {
                expect(recdUser.accountName.toLowerCase()).to.equal(accountName.toLowerCase());
                expect(recdUser.password).to.equal(sentUser.password);

                done();
              });
            });
        });

    });

    it('should update an existing user and return 204', function(done) {

      var accountName = '3mcommand';
      server.get('/users/xml/' + accountName + '?apikey=' + config.get('Service.apiKey'))
        .send()
        .expect('Content-Type', /xml/)
        .expect(200)
        .end(function(err, res) {
          if (err) return done(err);

          User.fromXml(res.text, function(err, sentUser) {
            if (err) return done(err);

            expect(sentUser.accountName.toLowerCase()).to.equal(accountName.toLowerCase());
            sentUser.password = 'newpass';
            sentUser.homeFolder = path.join(config.get('Rumpus.ftpRoot'), sentUser.accountName);

            server.put('/users/xml/' + accountName + '?apikey=' + config.get('Service.apiKey'))
              .set('Content-Type', 'application/xml')
              .send(sentUser.toXml())
              .expect(204)
              .end(function(err, res) {
                if (err) return done(err);

                // Retrieve the user again and verify its password was changed
                server.get('/users/xml/' + accountName + '?apikey=' + config.get('Service.apiKey'))
                  .send()
                  .expect('Content-Type', /xml/)
                  .expect(200)
                  .end(function(err, res) {
                    if (err) return done(err);

                    User.fromXml(res.text, function(err, recdUser) {

                      expect(recdUser).to.be.an('Object');
                      expect(recdUser.accountName.toLowerCase()).to.equal(accountName.toLowerCase());
                      expect(recdUser.password).to.equal(sentUser.password);

                      done();
                    });
                  });
              });
          });
        });
    });
  });

  describe('DELETE /users/xml', function() {
    it('should return 404 if no user given', function(done) {
      server.delete('/users/xml?apikey=' + config.get('Service.apiKey'))
        .send()
        .expect(404)
        .end(done);
    });

    it('should reject missing api key', function(done) {
      server.delete('/users/xml/nosuchuser')
        .send()
        .expect(401)
        .end(done);
    });

    it('should reject bad api key', function(done) {
      server.delete('/users/xml/nosuchuser?apikey=nosuchkey')
        .send()
        .expect(401)
        .end(done);
    });

    it('should return 404 if user not found', function(done) {
      server.delete('/users/xml/nosuchuser?apikey=' + config.get('Service.apiKey'))
        .send()
        .expect(404)
        .end(done);
    });

    it('should return 403 if user is protected', function(done) {
      var protected = config.get('ProtectedAccounts');

      server.delete('/users/xml/' + protected[0] + '?apikey=' + config.get('Service.apiKey'))
        .send()
        .expect(403)
        .end(done);
    });

    it('should delete an existing user and return 204', function(done) {

      var accountName = '3mcommand';
      server.get('/users/xml/' + accountName + '?apikey=' + config.get('Service.apiKey'))
        .send()
        .expect('Content-Type', /xml/)
        .expect(200)
        .end(function(err, res) {
          if (err) return done(err);

          User.fromXml(res.text, function(err, sentUser) {
            if (err) return done(err);

            expect(sentUser.accountName.toLowerCase()).to.equal(accountName.toLowerCase());

            server.delete('/users/xml/' + accountName + '?apikey=' + config.get('Service.apiKey'))
              .send()
              .expect(204)
              .end(function(err, res) {
                if (err) return done(err);

                // Retrieve the user again and verify it's gone
                server.get('/users/xml/' + accountName + '?apikey=' + config.get('Service.apiKey'))
                  .send()
                  .expect(404)
                  .end(done);
              });
          });
        });
    });

  });
});