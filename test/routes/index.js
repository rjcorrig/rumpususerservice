var expect = require('chai').expect;
var request = require('supertest');
var app = require('../../app');

var initApp = function() {
  server = request.agent(app);
};

describe('/', function() {
  beforeEach(initApp);

  describe('GET /', function() {
    it('returns "Service available"', function(done) {
      server.get('/')
        .send()
        .expect('Content-Type', /html/)
        .expect(200)
        .end(function(err, res) {
          if (err) return done(err);

          expect(res.text).to.contain('Service available');

          done();
        });      
    });
  });
});