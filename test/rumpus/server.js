var Server = require('../../rumpus/server');
var expect = require('chai').expect;
var config = require('config');
var fs = require('fs');
var path = require('path');
var rimraf = require('rimraf');
var User = require('../../rumpus/user');
var Notice = require('../../rumpus/notice');
var lastUserName, numUsers;

var ftpRoot = config.get('Rumpus.ftpRoot');

var initApp = function() {
  if (process.env.NODE_ENV === 'production')
    throw new Error('Tests must only run in development mode');

  // Reload original test files
  var testUserDb = path.join(__dirname, '../rumpus/Rumpus.users.test');
  var userDb = fs.readFileSync(testUserDb);
  var testNoticeDb = path.join(__dirname, '../rumpus/Rumpus.notices.test');
  var noticeDb = fs.readFileSync(testNoticeDb);

  // Create test config path if it doesn't exist
  var configPath = config.get('Rumpus.configPath');
  if (!fs.existsSync(configPath))
    fs.mkdirSync(configPath);

  fs.writeFileSync(path.join(configPath, 'Rumpus.users'), userDb);
  fs.writeFileSync(path.join(configPath, 'Rumpus.notices'), noticeDb);
  
  var users = userDb.toString().trim().split('\r');
  lastUserName = users[users.length-1].split('\t')[0];
  numUsers = users.length;

  var notices = noticeDb.toString().trim().split('\r').filter(function (notice) {
    // Only process csv rows with [1] == 1 (e-mail notice)
    return notice.split('\t')[1] == 1;
  });
  lastNoticeName = notices[notices.length-1].split('\t')[0];
  numNotices = notices.length;

  // Make sure the test folder exists
  if (!fs.existsSync(ftpRoot)) 
    fs.mkdirSync(ftpRoot);

  // Remove any created home folders
  fs.readdirSync(ftpRoot)
    .map(function(dir) { 
      return path.join(ftpRoot, dir); 
    })
    .forEach(function(file) {
      rimraf.sync(file);
    });

  // Setup for makeHomeFolder
  fs.mkdirSync(path.join(ftpRoot, 'existingDir'));

  fs.writeFileSync(path.join(ftpRoot, 'existingFile'), 'some data');

};

describe('server', function() {
  beforeEach(initApp);
  
  describe('constructor', function() {
    it('has a default', function(done) {
      var server = new Server();
      expect(server.getUserDb()).to.not.equal(undefined);
      done();
    });
    it('accepts a userDb path', function(done) {
      var setup = { userDb: path.join(__dirname, 'Rumpus.users.test') };
      var server = new Server(setup);
      expect(server.getUserDb()).to.equal(setup.userDb);
      done();
    });
  });

  describe('findUser', function() {
    it('returns the named user if it exists', function(done) {
      var server = new Server();
      var accountName = 'ANONYMOUS';

      server.findUser(accountName, function(err, user) {
        if (err) return done(err);

        expect(user).to.not.be.undefined;
        expect(user.accountName).to.equal(accountName);

        done();
      });
    });

    it('returns the named user if it exists, regardless of case', function(done) {
      var server = new Server();
      var accountName = 'AnoNyMouS';

      server.findUser(accountName, function(err, user) {
        if (err) return done(err);

        expect(user).to.not.be.undefined;
        expect(user.accountName.toLowerCase()).to.equal(accountName.toLowerCase());

        done();
      });
    });

    it('returns undefined if the user does not exist', function(done) {
      var server = new Server();

      server.findUser('nosuchuser', function(err, user) {
        if (err) return done(err);

        expect(user).to.be.undefined;

        done();
      });
    });

  });

  describe('getUsers', function() {
    it('retrieves an array of Users', function(done) {
      var server = new Server();

      server.getUsers(function(err, results) {
        if (err) 
          return done(err);

        expect(results).to.be.an('array');
        expect(results[0]).to.be.an('object');
        expect(results[0]).to.have.property('accountName');
        expect(results.length).to.equal(numUsers);
        expect(results[results.length-1].accountName).to.equal(lastUserName);
        done();
      });
    });

    it('retrieves all Users when given a {} predicate', function(done) {
      var server = new Server();

      server.getUsers(function(err, results) {
        if (err) 
          return done(err);

        expect(results).to.be.an('array');
        expect(results[0]).to.be.an('object');
        expect(results[0]).to.have.property('accountName');
        expect(results.length).to.equal(numUsers);
        expect(results[results.length-1].accountName).to.equal(lastUserName);
        done();
      }, {});
    });

    it('retrieves an array of Users matching a predicate', function(done) {
      var server = new Server();
      var uploadNoticeName = 'Administrator';

      server.getUsers(function(err, results) {
        if (err) 
          return done(err);

        expect(results).to.be.an('array');
        expect(results[0]).to.be.an('object');
        expect(results.length).to.equal(28);
        expect(results[0].uploadNoticeName).to.equal(uploadNoticeName);
        expect(results[results.length-1].uploadNoticeName).to.equal(uploadNoticeName);
        done();
      }, { uploadNoticeName: uploadNoticeName });
    });

    it('returns an empty array if no Users match the supplied predicate', function(done) {
      var server = new Server();

      server.getUsers(function(err, results) {
        if (err) 
          return done(err);

        expect(results).to.be.an('array');
        expect(results.length).to.equal(0);
        done();
      }, { uploadNoticeName: 'xyzzy' });
    });
  });

  describe('getUsersSync', function() {
    it('retrieves an array of Users', function(done) {
      var server = new Server();

      try {
        var results = server.getUsersSync();

        expect(results).to.be.an('array');
        expect(results[0]).to.be.an('object');
        expect(results[0]).to.have.property('accountName');
        expect(results.length).to.equal(numUsers);
        expect(results[results.length-1].accountName).to.equal(lastUserName);
        return done();
      } catch (err) {
        return done(err);
      }

    });

    it('retrieves all Users when given a {} predicate', function(done) {
      var server = new Server();

      try {
        var results = server.getUsersSync({});

        expect(results).to.be.an('array');
        expect(results[0]).to.be.an('object');
        expect(results[0]).to.have.property('accountName');
        expect(results.length).to.equal(numUsers);
        expect(results[results.length-1].accountName).to.equal(lastUserName);
        return done();
      } catch (err) {
        return done(err);
      }

    });

    it('retrieves an array of Users matching a predicate', function(done) {
      var server = new Server();
      var uploadNoticeName = 'Administrator';

      try {
        var results = server.getUsersSync({
          uploadNoticeName: uploadNoticeName
        });

        expect(results).to.be.an('array');
        expect(results[0]).to.be.an('object');
        expect(results.length).to.equal(28);
        expect(results[0].uploadNoticeName).to.equal(uploadNoticeName);
        expect(results[results.length-1].uploadNoticeName).to.equal(uploadNoticeName);

        return done();
      } catch (err) {
        return done(err);
      }

    });

    it('retrieves an empty array if no Users match the supplied predicate', function(done) {
      var server = new Server();

      try {
        var results = server.getUsersSync({
          uploadNoticeName: 'xyzzy'
        });

        expect(results).to.be.an('array');
        expect(results.length).to.equal(0);
        return done();
      } catch (err) {
        return done(err);
      }

    });

  });

  describe('findNotice', function() {
    it('returns the named notice if it exists', function(done) {
      var server = new Server();
      var noticeName = 'Administrator';

      server.findNotice(noticeName, function(err, notice) {
        if (err) return done(err);

        expect(notice).to.not.be.undefined;
        expect(notice.noticeName).to.equal(noticeName);

        done();
      });
    });

    it('returns the named notice if it exists, regardless of case', function(done) {
      var server = new Server();
      var noticeName = 'AdMiNisTraToR';

      server.findNotice(noticeName, function(err, notice) {
        if (err) return done(err);

        expect(notice).to.not.be.undefined;
        expect(notice.noticeName.toLowerCase()).to.equal(noticeName.toLowerCase());

        done();
      });
    });

    it('returns undefined if the notice does not exist', function(done) {
      var server = new Server();

      server.findNotice('nosuchnotice', function(err, notice) {
        if (err) return done(err);

        expect(notice).to.be.undefined;

        done();
      });
    });

  });

  describe('getNotices', function() {
    it('retrieves an array of Notices', function(done) {
      var server = new Server();

      server.getNotices(function(err, results) {
        if (err) 
          return done(err);

        expect(results).to.be.an('array');
        expect(results[0]).to.be.an('object');
        expect(results[0]).to.have.property('noticeName');
        expect(results.length).to.equal(numNotices);
        expect(results[results.length-1].noticeName).to.equal(lastNoticeName);
        done();
      });
    });

    it('retrieves all Notices when given a {} predicate', function(done) {
      var server = new Server();

      server.getNotices(function(err, results) {
        if (err) 
          return done(err);

        expect(results).to.be.an('array');
        expect(results[0]).to.be.an('object');
        expect(results[0]).to.have.property('noticeName');
        expect(results.length).to.equal(numNotices);
        expect(results[results.length-1].noticeName).to.equal(lastNoticeName);
        done();
      }, {});
    });

    it('retrieves an array of Notices matching a predicate', function(done) {
      var server = new Server();
      var onePerSession = 'N';

      server.getNotices(function(err, results) {
        if (err) 
          return done(err);

        expect(results).to.be.an('array');
        expect(results[0]).to.be.an('object');
        expect(results.length).to.equal(2);
        expect(results[0].onePerSession).to.equal(onePerSession);
        expect(results[results.length-1].onePerSession).to.equal(onePerSession);
        done();
      }, { onePerSession: onePerSession });
    });

    it('returns an empty array if no Notices match the supplied predicate', function(done) {
      var server = new Server();

      server.getNotices(function(err, results) {
        if (err) 
          return done(err);

        expect(results).to.be.an('array');
        expect(results.length).to.equal(0);
        done();
      }, { onePerSession: 'xyzzy' });
    });
  });

  describe('getNoticesSync', function () {
    it('retrieves an array of Notices', function(done) {
      var server = new Server();

      try {
        var results = server.getNoticesSync();

        expect(results).to.be.an('array');
        expect(results[0]).to.be.an('object');
        expect(results[0]).to.have.property('noticeName');
        expect(results.length).to.equal(numNotices);
        expect(results[results.length-1].noticeName).to.equal(lastNoticeName);
        return done();
      } catch (err) {
        return done(err);
      }

    });

    it('retrieves all Notices when given a {} predicate', function(done) {
      var server = new Server();

      try {
        var results = server.getNoticesSync({});

        expect(results).to.be.an('array');
        expect(results[0]).to.be.an('object');
        expect(results[0]).to.have.property('noticeName');
        expect(results.length).to.equal(numNotices);
        expect(results[results.length-1].noticeName).to.equal(lastNoticeName);
        return done();
      } catch (err) {
        return done(err);
      }

    });

    it('retrieves an array of Notices matching a predicate', function(done) {
      var server = new Server();
      var onePerSession = 'N';

      try {
        var results = server.getNoticesSync({
          onePerSession: onePerSession
        });

        expect(results).to.be.an('array');
        expect(results[0]).to.be.an('object');
        expect(results.length).to.equal(2);
        expect(results[0].onePerSession).to.equal(onePerSession);
        expect(results[results.length-1].onePerSession).to.equal(onePerSession);

        return done();
      } catch (err) {
        return done(err);
      }

    });

    it('retrieves an empty array if no Notices match the supplied predicate', function(done) {
      var server = new Server();

      try {
        var results = server.getNoticesSync({
          onePerSession: 'xyzzy'
        });

        expect(results).to.be.an('array');
        expect(results.length).to.equal(0);
        return done();
      } catch (err) {
        return done(err);
      }

    });
  });

  describe('makeHomeFolder', function() {
    it('allows setting the ftp root folder to the home folder', function(done) {
      var server = new Server();
      var homeFolder = ftpRoot;

      try {
        server.makeHomeFolder(homeFolder);

        fs.stat(homeFolder, function(err, stats) {
          if (err) return done(err);

          expect(stats.isDirectory()).to.be.true;

          return done();
        });
        
      } catch (err) {
        return done(err);
      }
    });

    it('returns quietly if the folder exists', function(done) {
      var server = new Server();
      var homeFolder = path.join(ftpRoot, 'existingDir');

      try {
        server.makeHomeFolder(homeFolder);

        fs.stat(homeFolder, function(err, stats) {
          if (err) return done(err);

          expect(stats.isDirectory()).to.be.true;

          return done();
        });
        
      } catch (err) {
        return done(err);
      }
    });

    it('creates the folder if it doesn\'t exist', function(done) {
      var server = new Server();
      var homeFolder = path.join(ftpRoot, 'newDir');

      try {
        server.makeHomeFolder(homeFolder);

        fs.stat(homeFolder, function(err, stats) {
          if (err) return done(err);

          expect(stats.isDirectory()).to.be.true;

          return done();
        });
        
      } catch (err) {
        return done(err);
      }
    });

    it('creates the folder recursively if its parent doesn\'t exist', function(done) {
      var server = new Server();
      var homeFolder = path.join(ftpRoot, 'newDir', 'newSub');

      try {
        server.makeHomeFolder(homeFolder);

        fs.stat(homeFolder, function(err, stats) {
          if (err) return done(err);

          expect(stats.isDirectory()).to.be.true;

          return done();
        });
        
      } catch (err) {
        return done(err);
      }
    });

    it('fails if the file at the given path isn\'t a folder', function(done) {
      var server = new Server();
      var homeFolder = path.join(ftpRoot, 'existingFile');

      expect(server.makeHomeFolder.bind(server, homeFolder)).to.throw(/is not a directory/);

      return done();
    });

    it('fails if the given homeFolder is not a child of the FTP root', function(done) {
      var server = new Server();
      var homeFolder = path.join('/usr/local/badPath');

      expect(server.makeHomeFolder.bind(server, homeFolder)).to.throw(/homeFolder must be the FTP root or a child directory/);

      return done();
    });
  });

  describe('reloadUserDatabase', function() {
    this.timeout(5000);
    it('reloads db on correct url', function(done) {
      var server = new Server();
      server.reloadUserDatabase(function(err, result) {
        if (err)
          return done(err);

        expect(result).to.equal(true);
        done();
      });
    });

    it('fails on bad path', function(done) {
      var server = new Server({ reloadUrl: config.get('Rumpus.reloadUrl') + 'bad' });
      server.reloadUserDatabase(function(err, result) {

        expect(err).to.not.equal(null);
        expect(result).to.equal(null);
        done();
      });
    });

    it('fails on bad server', function(done) {
      var server = new Server({ reloadUrl: 'http://foo.bar/baz' });
      server.reloadUserDatabase(function(err, result) {

        expect(err).to.not.equal(null);
        expect(result).to.equal(null);
        done();
      });
    });

    it('uses https when specified', function(done) {
      var server = new Server({ reloadUrl: config.get('Rumpus.reloadUrl').replace('http://','https://')});
      server.reloadUserDatabase(function(err, result) {
        if (err)
          return done(err);

        expect(result).to.equal(true);
        done();
      });
    });

  });

  describe('save', function() {
    it('rejects a user that is invalid', function(done) {
      var server = new Server();

      var user = new User();

      server.save(user, function(err, created) {
        expect(err).to.not.be.undefined;
        expect(created).to.be.undefined;

        done();
      });
    });

    it('creates a new user and its home folder', function(done) {
      var server = new Server();

      var userConfig = {
        accountName: 'rjcorrig',
        password: 'plaintext',
        homeFolder: path.join(ftpRoot, 'rjcorrig', 'sub'),
        uploadNoticeName: 'Wadsworth Artwork',
        userEmailAddress: 'rjcorrig@gmail.com'
      };

      var user = new User(userConfig);      

      server.save(user, function(err, created) {
        if (err) return done(err);

        expect(created).to.be.true;
        server.getUsers(function(err, users) {
          if (err) return done(err);

          expect(users.length).to.equal(numUsers+1);
          expect(users[users.length-1].accountName).to.equal(lastUserName);
          server.findUser(user.accountName, function(err, savedUser) {
            if (err) return done(err);

            expect(savedUser).to.not.be.undefined;
            for (var key in user) {
              expect(savedUser).to.have.property(key);
              expect(savedUser[key]).to.equal(user[key]);
            }

            fs.stat(userConfig.homeFolder, function(err, stats) {
              if (err) return done(err);

              expect(stats.isDirectory()).to.be.true;

              done();
            });
          });
        });
      });
    });

    it('updates an existing user and ensures its home folder exists', function(done) {
      var server = new Server();
      server.findUser('3mcommand', function(err, user) {
        if (err) return done(err);

        user.password = 'plaintext';
        user.homeFolder = path.join(ftpRoot, '3mcommand', 'sub');
        server.save(user, function(err, created) {
          if (err) return done(err);

          expect(created).to.be.false;
          server.getUsers(function(err, users) {
            if (err) return done(err);

            expect(users.length).to.equal(numUsers);
            expect(users[users.length-1].accountName).to.equal(lastUserName);

            server.findUser(user.accountName, function(err, savedUser) {
              if (err) return done(err);

              expect(savedUser).to.not.be.undefined;
              for (var key in user) {
                expect(savedUser).to.have.property(key);
                expect(savedUser[key]).to.equal(user[key]);
              }

              fs.stat(savedUser.homeFolder, function(err, stats) {
                if (err) return done(err);

                expect(stats.isDirectory()).to.be.true;

                done();
              });
            });

          });
        });
      });

    });
  });

  describe('delete', function() {
    it('rejects protected users with "denied"', function(done) {
      var server = new Server();

      server.delete('Administrator', function(err) {
        expect(err).to.not.be.undefined;
        expect(err).to.be.an('Error');
        expect(err.message).to.contain('denied');
        done();
      });

    });

    it('rejects an unknown user with "not found"', function(done) {
      var server = new Server();

      server.delete('nosuchuser', function(err) {
        expect(err).to.not.be.undefined;
        expect(err).to.be.an('Error');
        expect(err.message).to.contain('not found');
        done();
      });

    });

    it ('deletes a user if found', function(done) {
      var server = new Server();
      var accountName = '3mcommand';

      server.delete(accountName, function(err) {
        if (err) return done(err);

        server.getUsers(function(err, users) {
          if (err) return done(err);

          expect(users.length).to.equal(numUsers-1);
          expect(users[users.length-1].accountName).to.equal(lastUserName);

          server.findUser(accountName, function(err, user) {
            if (err) return done(err);

            expect(user).to.be.undefined;

            done();
          });
        });
      });
    });

  });
    
});