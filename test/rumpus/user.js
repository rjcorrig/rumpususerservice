var User = require('../../rumpus/user');
var expect = require('chai').expect;
var config = require('config');
var xmlbuilder = require('xmlbuilder');
var path = require('path');

describe('User', function() {
  describe('constructor', function() {

    it('receives all properties', function(done) {
      var userConfig = {
        accountName: 'rjcorrig',
        password: 'plaintext',
        homeFolder: '/FTP Drops/rjcorrig',
        uploadNoticeName: 'Wadsworth Artwork',
        userEmailAddress: 'rjcorrig@gmail.com'
      };

      var user = new User(userConfig);
      
      for (var key in userConfig) {
        expect(user).to.have.property(key);
        expect(user[key]).to.equal(userConfig[key]);
      }
      
      done();
    });

    it('defaults all properties', function(done) {

      var user = new User();
      
      expect(user).to.have.property('accountName');
      expect(user.accountName).to.equal('');

      expect(user).to.have.property('customerNumber');
      expect(user.customerNumber).to.equal('');

      expect(user).to.have.property('password');
      expect(user.password).to.equal('');

      expect(user).to.have.property('homeFolder');
      expect(user.homeFolder).to.equal(config.get('Rumpus.ftpRoot'));

      expect(user).to.have.property('permissions');
      expect(user.permissions).to.equal('YYYYYYYYNNNYYYNN');

      expect(user).to.have.property('maxFolderSize');
      expect(user.maxFolderSize).to.equal('0');
      
      expect(user).to.have.property('folderSetId');
      expect(user.folderSetId).to.equal('0');

      expect(user).to.have.property('uploadNoticeName');
      expect(user.uploadNoticeName).to.equal(config.get('Rumpus.defaultUploadNotice'));

      expect(user).to.have.property('maxSimultaneousConnections');
      expect(user.maxSimultaneousConnections).to.equal('N1');

      expect(user).to.have.property('maxUploadRate');
      expect(user.maxUploadRate).to.equal('N16');

      expect(user).to.have.property('maxUploadDownloadRatio');
      expect(user.maxUploadDownloadRatio).to.equal('N10');

      expect(user).to.have.property('customFilePermissionSettings');
      expect(user.customFilePermissionSettings).to.equal('NNNN');

      expect(user).to.have.property('accountExpirationInfo');
      expect(user.accountExpirationInfo).to.equal('P');

      expect(user).to.have.property('maxDownloadRate');
      expect(user.maxDownloadRate).to.equal('N16');

      expect(user).to.have.property('customOwner');
      expect(user.customOwner).to.equal('N-');

      expect(user).to.have.property('downloadNoticeName');
      expect(user.downloadNoticeName).to.equal('');

      expect(user).to.have.property('userEmailAddress');
      expect(user.userEmailAddress).to.equal('');

      expect(user).to.have.property('uploadCenter');
      expect(user.uploadCenter).to.equal('');

      expect(user).to.have.property('alternateWFMAppearance');
      expect(user.alternateWFMAppearance).to.equal('.');

      expect(user).to.have.property('secondaryUploadNotice');
      expect(user.secondaryUploadNotice).to.equal('');
      
      done();
    });

    it('accepts all properties when options.complete is true', function(done) {

      var userConfig = {
        accountName: '3mcommand',
        password: 'newpass',
        homeFolder: 'RumpusTest\FTP Drops\3mcommand',
        permissions: 'YYYYYYYYNNN',
        maxFolderSize: '0',
        folderSetId: '0',
        uploadNoticeName: 'Wadsworth Artwork',
        maxSimultaneousConnections: 'N1',
        maxUploadRate: 'N16',
        maxUploadDownloadRatio: 'N10',
        customFilePermissionSettings: 'NNNN',
        accountExpirationInfo: 'P',
        maxDownloadRate: 'N16',
        customOwner: 'N-',
        downloadNoticeName: '',
        userEmailAddress: 'weherzog1@mmm.com',
        uploadCenter: '',
        alternateWFMAppearance: '.',
        secondaryUploadNotice: ''
      };

      var user = new User(userConfig, { complete: true });
      
      for (var key in userConfig) {
        expect(user).to.have.property(key);
        expect(user[key]).to.equal(userConfig[key]);
      }
      
      done();
    });

    it('throws if the userConfig object is incomplete and options.complete is true', function(done) {

      var userConfig = {
        accountName: 'rjcorrig',
        password: 'plaintext',
        homeFolder: '/FTP Drops/rjcorrig',
        uploadNoticeName: 'Wadsworth Artwork',
        userEmailAddress: 'rjcorrig@gmail.com'
      };

      expect(User.bind(null, userConfig, { complete: true })).to.throw(/is required/);
      
      done();

    });
    
    it('translates ROOT homeFolder to configured root', function(done) {
      var userConfig = {
        accountName: 'rjcorrig',
        password: 'plaintext',
        homeFolder: 'ROOT',
        uploadNoticeName: 'Wadsworth Artwork',
        userEmailAddress: 'rjcorrig@gmail.com'
      };

      var user = new User(userConfig);
      
      expect(user.homeFolder).to.equal(config.get('Rumpus.ftpRoot'));
      
      done();
    });

  });

  describe('customerNumber', function() {
    it ('is readonly', function(done) {

      var userConfig = {
        accountName: 'rjcorrig',
        password: 'plaintext',
        homeFolder: path.join(config.get('Rumpus.ftpRoot'), '987654'),
        uploadNoticeName: 'Wadsworth Artwork',
        userEmailAddress: 'rjcorrig@gmail.com'
      };

      var user = new User(userConfig);
      
      expect(user).to.have.property('customerNumber');
      expect(user.customerNumber).to.equal('987654');
      user.customerNumber = '123456';
      expect(user.customerNumber).to.equal('987654');

      done();

    });

    it('defaults to blank line if subfolder of root is non-numeric', function(done) {

      var userConfig = {
        accountName: 'rjcorrig',
        password: 'plaintext',
        homeFolder: path.join(config.get('Rumpus.ftpRoot'), 'rjcorrig'),
        uploadNoticeName: 'Wadsworth Artwork',
        userEmailAddress: 'rjcorrig@gmail.com'
      };

      var user = new User(userConfig);
      
      expect(user).to.have.property('customerNumber');
      expect(user.customerNumber).to.equal('');

      done();

    });

    it('sets to subfolder of ROOT if numeric', function(done) {

      var userConfig = {
        accountName: 'rjcorrig',
        password: 'plaintext',
        homeFolder: path.join(config.get('Rumpus.ftpRoot'), '987654'),
        uploadNoticeName: 'Wadsworth Artwork',
        userEmailAddress: 'rjcorrig@gmail.com'
      };

      var user = new User(userConfig);
      
      expect(user).to.have.property('customerNumber');
      expect(user.customerNumber).to.equal('987654');

      done();

    });

    it('removes leading zeros from given customerNumber', function(done) {

      var userConfig = {
        accountName: 'rjcorrig',
        password: 'plaintext',
        homeFolder: path.join(config.get('Rumpus.ftpRoot'), '0987654'),
        uploadNoticeName: 'Wadsworth Artwork',
        userEmailAddress: 'rjcorrig@gmail.com'
      };

      var user = new User(userConfig);
      
      expect(user).to.have.property('customerNumber');
      expect(user.customerNumber).to.equal('987654');

      done();

    });


    it('sets to subfolder of ROOT if top level of home folder is numeric', function(done) {

      var userConfig = {
        accountName: 'rjcorrig',
        password: 'plaintext',
        homeFolder: path.join(config.get('Rumpus.ftpRoot'), '987654/subcustomer'),
        uploadNoticeName: 'Wadsworth Artwork',
        userEmailAddress: 'rjcorrig@gmail.com'
      };

      var user = new User(userConfig);
      
      expect(user).to.have.property('customerNumber');
      expect(user.customerNumber).to.equal('987654');

      done();

    });

  });

  describe('toXml', function() {
    it('returns all properties', function(done) {
      var userConfig = new User();
      var xml = userConfig.toXml();

      expect(xml).to.have.string('<customerNumber/>');

      User.fromXml(xml, function(err, user) {
        if (err) return done(err);
      
        for (var key in userConfig) {
          expect(user).to.have.property(key);
          expect(user[key]).to.equal(userConfig[key]);
        }
        
        done();        
      });
    });
  });

  describe('fromXml', function() {
    it('requires <user> root node', function(done) {
      var xml = xmlbuilder.create({root: {}}).end();
      User.fromXml(xml, function(err, user) {
        expect(err).to.not.be.undefined;
        expect(user).to.be.undefined;

        done();
      });
    });

    it('returns a user from a partial XML file', function(done) {

      var userConfig = {
        accountName: 'rjcorrig',
        password: 'plaintext',
        homeFolder: '/FTP Drops/rjcorrig',
        uploadNoticeName: 'Wadsworth Artwork',
        userEmailAddress: 'rjcorrig@gmail.com'
      };

      var xml = xmlbuilder.create({ user: userConfig }).end();
      User.fromXml(xml, function(err, user) {
        if (err) return done(err);
      
        expect(user).to.have.property('accountName');
        expect(user.accountName).to.equal(userConfig.accountName);

        expect(user).to.have.property('password');
        expect(user.password).to.equal(userConfig.password);

        expect(user).to.have.property('homeFolder');
        expect(user.homeFolder).to.equal(userConfig.homeFolder);

        expect(user).to.have.property('permissions');
        expect(user.permissions).to.equal('YYYYYYYYNNNYYYNN');

        expect(user).to.have.property('maxFolderSize');
        expect(user.maxFolderSize).to.equal('0');
        
        expect(user).to.have.property('folderSetId');
        expect(user.folderSetId).to.equal('0');

        expect(user).to.have.property('uploadNoticeName');
        expect(user.uploadNoticeName).to.equal(userConfig.uploadNoticeName);

        expect(user).to.have.property('maxSimultaneousConnections');
        expect(user.maxSimultaneousConnections).to.equal('N1');

        expect(user).to.have.property('maxUploadRate');
        expect(user.maxUploadRate).to.equal('N16');

        expect(user).to.have.property('maxUploadDownloadRatio');
        expect(user.maxUploadDownloadRatio).to.equal('N10');

        expect(user).to.have.property('customFilePermissionSettings');
        expect(user.customFilePermissionSettings).to.equal('NNNN');

        expect(user).to.have.property('accountExpirationInfo');
        expect(user.accountExpirationInfo).to.equal('P');

        expect(user).to.have.property('maxDownloadRate');
        expect(user.maxDownloadRate).to.equal('N16');

        expect(user).to.have.property('customOwner');
        expect(user.customOwner).to.equal('N-');

        expect(user).to.have.property('downloadNoticeName');
        expect(user.downloadNoticeName).to.equal('');

        expect(user).to.have.property('userEmailAddress');
        expect(user.userEmailAddress).to.equal(userConfig.userEmailAddress);

        expect(user).to.have.property('uploadCenter');
        expect(user.uploadCenter).to.equal('');

        expect(user).to.have.property('alternateWFMAppearance');
        expect(user.alternateWFMAppearance).to.equal('.');

        expect(user).to.have.property('secondaryUploadNotice');
        expect(user.secondaryUploadNotice).to.equal('');
        
        done();
      });
    });  
    
  });

  describe('validate', function() {
    it('returns no error if the User is valid', function(done) {
      var userConfig = {
        accountName: 'rjcorrig'
      };

      var user = new User(userConfig);
      user.validate(function (err) {
        if (err) return done(err);

        return done();
      });
    });

    it('returns an error if the user has no accountName', function(done) {

      var user = new User({});
      user.validate(function (err) {
        expect(err).to.not.be.undefined;
        expect(err).to.equal('accountName is required');

        done();
      });
    });

    it('returns an error if the homeFolder is not the configured root or its child', function(done) {

      var user = new User({
        accountName: 'rjcorrig',
        homeFolder: '/'
      });
      user.validate(function (err) {
        expect(err).to.not.be.undefined;
        expect(err).to.equal('homeFolder must be the FTP root or a child directory');

        done();
      });
    });

    it('returns an error if the userEmailAddress is not a valid e-mail', function(done) {

      var user = new User({
        accountName: 'rjcorrig',
        userEmailAddress: 'Robert Corrigan <corrigan@rohrer.com>'
      });
      user.validate(function (err) {
        expect(err).to.not.be.undefined;
        expect(err).to.match(/is not a valid e-mail address/);

        done();
      });

    });

    it('allows a valid e-mail in userEmailAddress', function(done) {
      
      var user = new User({
        accountName: 'rjcorrig',
        userEmailAddress: 'corrigan@rohrer.com'
      });
      user.validate(done);

    });
      
    it('allows a blank userEmailAddress', function(done) {
      
      var user = new User({
        accountName: 'rjcorrig',
        userEmailAddress: ''
      });
      user.validate(done);

    });
      
  });
});

