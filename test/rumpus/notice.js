var Notice = require('../../rumpus/notice');
var expect = require('chai').expect;
var xmlbuilder = require('xmlbuilder');

describe('Notice', function () {
  describe('constructor', function () {
    it('receives all properties', function(done) {
      var noticeConfig = {
        noticeName: 'rjcorrig',
        onePerSession: 'N',
        mailServer: 'ns1.rohrer.com',
        mailFrom: 'rumpus@rohrer.com',
        mailTo: 'rjcorrig@gmail.com',
        mailCC: 'rjcorrig@yahoo.com',        
        mailSubject: 'FTP File Drop Notification' 
      };

      var notice = new Notice(noticeConfig);
      
      for (var key in noticeConfig) {
        expect(notice).to.have.property(key);
        expect(notice[key]).to.equal(noticeConfig[key]);
      }
      
      done();
    });

    it('defaults all properties', function(done) {

      var notice = new Notice();
      
      expect(notice).to.have.property('noticeName');
      expect(notice.noticeName).to.equal('');

      expect(notice).to.have.property('onePerSession');
      expect(notice.onePerSession).to.equal('N');

      expect(notice).to.have.property('mailServer');
      expect(notice.mailServer).to.equal('');

      expect(notice).to.have.property('mailFrom');
      expect(notice.mailFrom).to.equal('');

      expect(notice).to.have.property('mailTo');
      expect(notice.mailTo).to.equal('');

      expect(notice).to.have.property('mailCC');
      expect(notice.mailCC).to.equal('');
      
      expect(notice).to.have.property('mailSubject');
      expect(notice.mailSubject).to.equal('');
      
      done();
    });
  });

  describe('toXml', function () {
    it ('returns all properties', function (done) {
      var noticeConfig = {
        noticeName: 'rjcorrig',
        onePerSession: 'N',
        mailServer: 'ns1.rohrer.com',
        mailFrom: 'rumpus@rohrer.com',
        mailTo: 'rjcorrig@gmail.com',
        mailCC: 'rjcorrig@yahoo.com',
        mailSubject: 'FTP File Drop Notification' 
      };
  
      var notice = new Notice(noticeConfig);
      var xml = notice.toXml();

      Notice.fromXml(xml, function(err, notice) {
        if (err) return done(err);
      
        for (var key in noticeConfig) {
          expect(notice).to.have.property(key);
          expect(notice[key]).to.equal(noticeConfig[key]);
        }
        
        done();        
      });      

    });
  });

  describe('fromXml', function() {
    it('requires <notice> root node', function(done) {
      var xml = xmlbuilder.create({root: {}}).end();
      Notice.fromXml(xml, function(err, notice) {
        expect(err).to.not.be.undefined;
        expect(notice).to.be.undefined;

        done();
      });
    });

    it('returns a notice from a partial XML file', function(done) {

      var noticeConfig = {
        noticeName: 'Test Notice',
        onePerSession: 'Y',
        mailServer: 'mail.example.org',
        mailFrom: 'from@exmaple.org',
        mailTo: 'to1@example.org,to2@example.org',
        mailCC: 'cc1@example.org,cc2@example.org',
        mailSubject: 'Test Subject'
      };

      var xml = xmlbuilder.create({ notice: noticeConfig }).end();
      Notice.fromXml(xml, function(err, notice) {
        if (err) return done(err);
      
        expect(notice).to.have.property('noticeName');
        expect(notice.noticeName).to.equal(noticeConfig.noticeName);

        expect(notice).to.have.property('onePerSession');
        expect(notice.onePerSession).to.equal(noticeConfig.onePerSession);

        expect(notice).to.have.property('mailServer');
        expect(notice.mailServer).to.equal(noticeConfig.mailServer);

        expect(notice).to.have.property('mailFrom');
        expect(notice.mailFrom).to.equal(noticeConfig.mailFrom);

        expect(notice).to.have.property('mailTo');
        expect(notice.mailTo).to.equal(noticeConfig.mailTo);

        expect(notice).to.have.property('mailCC');
        expect(notice.mailCC).to.equal(noticeConfig.mailCC);

        expect(notice).to.have.property('mailSubject');
        expect(notice.mailSubject).to.equal(noticeConfig.mailSubject);

        done();
      });
    });  
    
  });


  describe('fronCsvRow', function () {

    it('accepts an array with array[1] == 1', function(done) {
      var noticeColumns = ['a','1'];

      expect(Notice.fromCsvRow.bind(null, noticeColumns)).to.not.throw();
      done();
    });
      
    it('rejects an array with array[1] != 1', function(done) {
      var noticeColumns = ['a','2'];

      expect(Notice.fromCsvRow.bind(null, noticeColumns)).to.throw(/must be an e-mail/);
      done();      
    });

    it('parses all array elements in order', function(done) {
      var noticeColumns = ['a','1','c','d','e','f','g','h'];
      
      var notice = Notice.fromCsvRow(noticeColumns);
      expect(notice.noticeName).to.equal(noticeColumns[0]);
      expect(notice.onePerSession).to.equal(noticeColumns[2]);
      expect(notice.mailServer).to.equal(noticeColumns[3]);
      expect(notice.mailFrom).to.equal(noticeColumns[4]);
      expect(notice.mailTo).to.equal(noticeColumns[5]);
      expect(notice.mailCC).to.equal(noticeColumns[6]);
      expect(notice.mailSubject).to.equal(noticeColumns[7]);

      done();      
    });
  });
});
