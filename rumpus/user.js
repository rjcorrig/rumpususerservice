var config = require('config');
var parseString = require('xml2js').parseString;
var xmlbuilder = require('xmlbuilder');
var path = require('path');
var assert = require('assert');
var validator = require('email-validator');

/**
 * User module
 * 
 * @module rumpus/user
 */
module.exports = User;

/**
 * A user initialization Object  
 * @typedef {Object} UserInitializer
 * 
 * @property {string} [accountName] Login Name
 * @property {string} [password] User account password, plain text or encrypted
 * @property {string} [homeFolder] ROOT or full path to user home folder
 * @property {string} [permissions] The “Permissions” field is a single string of “Y/N” values representing the following permission values:<ul><li>Restrict To Home Folder</li><li>Permit Downloads</li><li>Permit Uploads</li><li>Permit Deletions</li><li>Permit Creation Of Folders</li><li>Permit Deletion Of Folders</li><li>Permit Login</li><li>Permit View Directories</li><li>See Invisible “Dot” Files (files with a name that begins with a period)</li><li>Administrator Privileges (allow user to manage user accounts)</li><li>Expire Files (delete files uploaded via Rumpus after specified time)</li>Permit FTP Access</li><li>Permit HTTP Access</li><li>Permit WebDAV Access</li><li>Permit Drop Shipping</li><li>Permit File Requests</li></ul>
 * @property {string} [maxFolderSize] Maximum folder size in MB
 * @property {string} [folderSetId] A numeric ID from the Rumpus.fsets file
 * @property {string} [uploadNoticeName] must match a defined notice name
 * @property {string} [maxSimultaneousConnections] Y or N to enable, followed by value
 * @property {string} [maxUploadRate] Y or N to enable, followed by value in Kbps
 * @property {string} [maxUploadDownloadRatio] Y or N to enable, followed by value
 * @property {string} [customFilePermissionSettings] Y or N to enable, plus "N" none, "R" read only, "B" read/write
 * @property {string} [accountExpirationInfo] P - permanent, D - disable, R - remove
 * @property {string} [maxDownloadRate] Y or N to enable, followed by value in Kbps
 * @property {string} [customOwner] Y or N determines parent folder, plus account name
 * @property {string} [downloadNoticeName] the name of an Upload Notice to be triggered for download
 * @property {string} [userEmailAddress] the user e-mail address
 * @property {string} [uploadCenter] the name of an Upload Center form for use with this user
 * @property {string} [alternateWFMAppearance] the domain name to be displayed
 * @property {string} [secondaryUploadNotice] a 2nd event notice may be triggered for uploads
 */

/**
 * Options for initializing a user  
 * @typedef {Object} UserInitializerOptions
 * 
 * @property {boolean} [complete] Require all fields to be passed in the UserInitializer object
*/

/** 
 * Create a User 
 * 
 * @constructor
 * @param {module:rumpus/user~UserInitializer} [args={}] - Values to initialize
 * @param {module:rumpus/user~UserInitializerOptions} [options={}] - Initialization options
 * 
 * @throws Will throw a 'fieldName is required' error if options.complete is true and the args object doesn't contain all writeable User properties
*/
function User(args, options) {
  args = args || {};
  options = options || {};

  if (options.complete) {
    var requiredArgs = [
      'accountName',
      'password',
      'homeFolder',
      'permissions',
      'maxFolderSize',
      'folderSetId',
      'uploadNoticeName',
      'maxSimultaneousConnections',
      'maxUploadRate',
      'maxUploadDownloadRatio',
      'customFilePermissionSettings',
      'accountExpirationInfo',
      'maxDownloadRate',
      'customOwner',
      'downloadNoticeName',
      'userEmailAddress',
      'uploadCenter',
      'alternateWFMAppearance',
      'secondaryUploadNotice'
    ];

    requiredArgs.forEach(function (key) {
      assert(args[key] !== undefined, key + ' is required');
    });
  }

  /**
   * Login Name
   * @member {string} 
   */
  this.accountName = args.accountName || '';
  /**
   * User account password, encrypted or plain text
   * @member {string} 
   */
  this.password = args.password || '';
  /**
   * Full path to user home folder
   * @member {string} 
   * @default The configured FTP root
   */
  this.homeFolder = args.homeFolder || config.get('Rumpus.ftpRoot');

  if (this.homeFolder.toUpperCase() === 'ROOT') {
    this.homeFolder = config.get('Rumpus.ftpRoot');
  }

  /**
   * The “Permissions” field is a single string of “Y/N” values representing the following permission values:<ul><li>Restrict To Home Folder</li><li>Permit Downloads</li><li>Permit Uploads</li><li>Permit Deletions</li><li>Permit Creation Of Folders</li><li>Permit Deletion Of Folders</li><li>Permit Login</li><li>Permit View Directories</li><li>See Invisible “Dot” Files (files with a name that begins with a period)</li><li>Administrator Privileges (allow user to manage user accounts)</li><li>Expire Files (delete files uploaded via Rumpus after specified time)</li>Permit FTP Access</li><li>Permit HTTP Access</li><li>Permit WebDAV Access</li><li>Permit Drop Shipping</li><li>Permit File Requests</li></ul>
   * @member {string} 
   * @default 'YYYYYYYYNNNYYYNN'
   */
  this.permissions = args.permissions || 'YYYYYYYYNNNYYYNN';

  /**
   * Maximum folder size in MB
   * @member {string}
   * @default '0'
   */
  this.maxFolderSize = args.maxFolderSize || '0';

  /**
   * A numeric ID from the Rumpus.fsets file
   * @member {string}
   * @default '0'
   */
  this.folderSetId = args.folderSetId || '0';

  /**
   * Notice triggered on upload: must match a defined notice name
   * @member {string}
   * @default The default value in the config file
   */
  this.uploadNoticeName = args.uploadNoticeName || config.get('Rumpus.defaultUploadNotice');

  /**
   * Y or N to enable, followed by value
   * @member {string}
   * @default 'N1'
   */
  this.maxSimultaneousConnections = args.maxSimultaneousConnections || 'N1';

  /**
   * Y or N to enable, followed by value in Kbps
   * @member {string}
   * @default 'N16'
   */
  this.maxUploadRate = args.maxUploadRate || 'N16';

  /**
   * Y or N to enable, followed by value
   * @member {string}
   * @default 'N10'
   */
  this.maxUploadDownloadRatio = args.maxUploadDownloadRatio || 'N10';

  /**
   * Y or N to enable, plus "N" none, "R" read or "B" read/write
   * @member {string}
   * @default 'NNNN'
   */
  this.customFilePermissionSettings = args.customFilePermissionSettings || 'NNNN';

  /**
   * P - permanent, D - disable, R - remove
   * @member {string}
   * @default 'P'
   */
  this.accountExpirationInfo = args.accountExpirationInfo || 'P';

  /**
   * Y or N to enable, followed by value in Kbps
   * @member {string}
   * @default 'N16'
   */
  this.maxDownloadRate = args.maxDownloadRate || 'N16';

  /**
   * Y or N determines parent folder, plus account name
   * @member {string}
   * @default 'N-'
   */
  this.customOwner = args.customOwner || 'N-';

  /**
   * The name of an upload notice to be triggered for downloads
   * @member {string}
   */
  this.downloadNoticeName = args.downloadNoticeName || '';

  /**
   * The e-mail address of the user
   * @member {string}
   */
  this.userEmailAddress = args.userEmailAddress || '';

  /**
   * The name of an Upload Center form for use by this user
   * @member {string}
   */
  this.uploadCenter = args.uploadCenter || '';

  /**
   * The domain name to be displayed
   * @member {string}
   * @default '.'
   */
  this.alternateWFMAppearance = args.alternateWFMAppearance || '.';

  /**
   * A 2nd event notice may be triggered for uploads
   * @member {string}
   */
  this.secondaryUploadNotice = args.secondaryUploadNotice || '';

  Object.defineProperties(this, {
    /**
     * Customer number, as defined by a numeric home folder under the root folder,
     * or a blank string if the home folder is not numeric.
     * 
     * Leading zeroes are stripped from the string
     * 
     * @type string
     * @readonly
     * @instance
     * @memberof module:rumpus/user~User
     */
    'customerNumber': {
      'enumerable': true,
      'get': function() {
        var subFolder = path.relative(config.get('Rumpus.ftpRoot'), this.homeFolder);
        var subFolderTop = subFolder.split(path.sep)[0];
        if (subFolderTop.length > 0 && !isNaN(subFolderTop)) {
          return Number(subFolderTop).toString();
        } else {
          return '';
        }
      }
    }
  });
}

/** 
 * Return a new User from a row 
 * 
 * @param {string[]} userColumns
 * @returns {module:rumpus/user~User}
*/

User.fromCsvRow = function(userColumns) {
  var user = new User({
    accountName: userColumns[0],
    password: userColumns[1],
    homeFolder: userColumns[2],
    permissions: userColumns[3],
    maxFolderSize: userColumns[4],
    folderSetId: userColumns[5],
    uploadNoticeName: userColumns[6],
    maxSimultaneousConnections: userColumns[7],
    maxUploadRate: userColumns[8],
    maxUploadDownloadRatio: userColumns[9],
    customFilePermissionSettings: userColumns[10],
    accountExpirationInfo: userColumns[11],
    maxDownloadRate: userColumns[12],
    customOwner: userColumns[13],
    downloadNoticeName: userColumns[14],
    userEmailAddress: userColumns[15],
    uploadCenter: userColumns[16],
    alternateWFMAppearance: userColumns[17],
    secondaryUploadNotice: userColumns[18],
  });
  return user;
};

/**
 * Return a tab delimited CSV row suitable for insertion into the Rumpus.users
 * database file
 * 
 * @returns {String}
 */
User.prototype.toCsvRow = function() {
  var userArray = [
    this.accountName,
    this.password,
    this.homeFolder,
    this.permissions,
    this.maxFolderSize,
    this.folderSetId,
    this.uploadNoticeName,
    this.maxSimultaneousConnections,
    this.maxUploadRate,
    this.maxUploadDownloadRatio,
    this.customFilePermissionSettings,
    this.accountExpirationInfo,
    this.maxDownloadRate,
    this.customOwner,
    this.downloadNoticeName,
    this.userEmailAddress,
    this.uploadCenter,
    this.alternateWFMAppearance,
    this.secondaryUploadNotice
  ];
  return userArray.join('\t');
};

/**
 * Create a user from an XML as defined in the Web API
 * 
 * @param {string} xml The XML to convert to a user
 * @param {userCallback} callback Error handler function (err, User)
 */
User.fromXml = function(xml, callback) {
  parseString(xml, { explicitArray: false }, function(err, result) {
    if (err) return callback(err);

    if (!result.user) return callback('No user XML was provided');

    return callback(null, new User(result.user));
  });
};

/**
 * Serialize this User as XML as defined in the Web API
 * 
 * @returns {string}
 */
User.prototype.toXml = function() {
  return xmlbuilder.create({user: this}).end();
};

User.prototype.validate = function(callback) {
  if (!this.accountName) {
    return callback('accountName is required');
  }

  if (this.userEmailAddress) {
    if (!validator.validate(this.userEmailAddress)) {
      return callback(this.userEmailAddress + ' is not a valid e-mail address');
    }
  }

  if (path.relative(config.get('Rumpus.ftpRoot'), this.homeFolder).substring(0,2) == '..') {
    return callback('homeFolder must be the FTP root or a child directory');
  }

  return callback();
};

