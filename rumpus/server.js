var config = require('config');
var path = require('path');
var url = require('url');
var http = require('http');
var https = require('https');
var fs = require('fs');
var { parse } = require('csv-parse');
var { parse: parseSync } = require('csv-parse/sync');
var { transform } = require('stream-transform');
var User = require('./user');
var Notice = require('./notice');
var _ = require('lodash');

/**
 * Server data access module
 * 
 * @module rumpus/server
 */
module.exports = Server;

/**
 * A configuration object to override default Server settings
 * 
 * @typedef {Object} Setup
 * @property {string} [userDb] Full path to Rumpus.users db file
 * @property {string} [reloadUrl] Url to trigger a server user database reload
 * @property {string} [noticeDb] Full path to Rumpus.notices db file
 */

/**
 * Server data access object
 * 
 * @constructor
 * @param {module:rumpus/server~Setup} [setup={}] - Configuration overrides
 */
function Server(setup) {
  setup = setup || {};

  this.userDb = setup.userDb || path.join(config.get('Rumpus.configPath'), 'Rumpus.users');
  this.reloadUrl = setup.reloadUrl || config.get('Rumpus.reloadUrl');
  this.noticeDb = setup.noticeDb || path.join(config.get('Rumpus.configPath'), 'Rumpus.notices');
}

/**
 * Find a User by accountName
 * 
 * @param {String} accountName - The name of the user to find
 * @param {userCallback} callback - Response handler (err, User)
 */
Server.prototype.findUser = function (accountName, callback) {
  this.getUsers(function(err, users) {
    if (err)
      return callback(err, null);

    return callback(null, users.find(function(user) {
      return user.accountName.toLowerCase() === accountName.toLowerCase();
    }));
  });
};

/**
 * Return the path of the Rumpus.users database file
 * 
 * @returns {String}
 */
Server.prototype.getUserDb = function() {
  return this.userDb;
};

/**
 * Return the path of the Rumpus.notices database file
 * 
 * @returns {String}
 */
Server.prototype.getNoticeDb = function() {
  return this.noticeDb;
};

/**
 * Return all Users in the database to the callback function as an array
 * 
 * @param {userArrayCallback} callback - Response handler (err, User[])
 * @param {function|Object} [filter] - a filter function or object
 */
Server.prototype.getUsers = function(callback, filter) {

  if (typeof filter !== 'function') {
    if (!filter || Object.keys(filter).length === 0) {
      filter = function() { return true; };
    }
  }

  fs.readFile(this.userDb, function(err, rows) {
    if (err)
      return callback(err, null);

    parse(rows, {
      delimiter: '\t', 
      record_delimiter: '\r',
      relax_quotes: true,
      relax_column_count: true
    }, function(err, userArray) {
      if (err)
        return callback(err, null);

      transform(userArray, User.fromCsvRow, function(err, output) {
        if (err)
          return callback(err, null);

        return callback(null, _.filter(output, filter));
      });
    });
  });
};

/**
 * Return all Users in the database as an array
 * 
 * @param {function|Object} [filter] - a filter function or object
 * @returns {module:rumpus/user~User[]}
 */
Server.prototype.getUsersSync = function(filter) {

  if (typeof filter !== 'function') {
    if (!filter || Object.keys(filter).length === 0) {
      filter = function() { return true; };
    }
  }

  var rows = fs.readFileSync(this.userDb);

  var userArray = parseSync(rows, {
    delimiter: '\t', 
    record_delimiter: '\r',
    relax_quotes: true,
    relax_column_count: true
  });

  var output = userArray.map(User.fromCsvRow);

  return _.filter(output, filter);
};

/**
 * Find a Notice by noticeName
 * 
 * @param {String} noticeName - The name of the notice to find
 * @param {noticeCallback} callback - Response handler (err, Notice)
 */
Server.prototype.findNotice = function (noticeName, callback) {
  this.getNotices(function(err, users) {
    if (err)
      return callback(err, null);

    return callback(null, users.find(function(notice) {
      return notice.noticeName.toLowerCase() === noticeName.toLowerCase();
    }));
  });
};

/**
 * Return all Notices in the database to the callback function as an array
 * 
 * @param {noticeArrayCallback} callback - Response handler (err, Notice[])
 * @param {function|Object} [filter] - a filter function or object
 */
Server.prototype.getNotices = function(callback, filter) {
  
  if (typeof filter !== 'function') {
    if (!filter || Object.keys(filter).length === 0) {
      filter = function() { return true; };
    }
  }

  fs.readFile(this.noticeDb, function(err, rows) {
    if (err)
      return callback(err, null);

    parse(rows, {
      delimiter: '\t', 
      record_delimiter: '\r',
      relax_quotes: true,
      relax_column_count: true
    }, function(err, noticeArray) {
      if (err)
        return callback(err, null);

      noticeArray = _.filter(noticeArray, function (row) {
        // Only return csv rows with type 1 (e-mail notice)
        return row[1] == 1;
      });

      transform(noticeArray, Notice.fromCsvRow, function(err, output) {
        if (err)
          return callback(err, null);

        return callback(null, _.filter(output, filter));
      });
    });
  });
};

/**
 * Return all Notices in the database as an array
 * 
 * @param {function|Object} [filter] - a filter function or object
 * @returns {module:rumpus/notice~Notice[]}
 */
Server.prototype.getNoticesSync = function(filter) {
  
  if (typeof filter !== 'function') {
    if (!filter || Object.keys(filter).length === 0) {
      filter = function() { return true; };
    }
  }

  var rows = fs.readFileSync(this.noticeDb);

  var noticeArray = parseSync(rows, {
    delimiter: '\t', 
    record_delimiter: '\r',
    relax_quotes: true,
    relax_column_count: true
  });

  var output = noticeArray.filter(function (row) {
    // Only return csv rows with type 1 (e-mail notice)
    return row[1] == 1;
  }).map(Notice.fromCsvRow);

  return _.filter(output, filter);
};

/**
 * Create a folder and all of its subfolders at the given path if it does not exist.
 * This function runs I/O synchronously.
 * 
 * @throws Will throw an error if the path is not a directory, or not a subfolder of the configured root
 * 
 * @param {string} homeFolder - the path of the directory to create
 */
Server.prototype.makeHomeFolder = function(homeFolder) {

  var rootFolder = config.get('Rumpus.ftpRoot');
  var relativePath = path.relative(rootFolder, homeFolder);

  if (relativePath.substring(0,2) == '..') {
    throw new Error('homeFolder must be the FTP root or a child directory');
  }

  var subfolders = relativePath.split(path.sep).reverse();

  homeFolder = rootFolder;
  while (subfolders.length > 0) {
    homeFolder = path.join(homeFolder, subfolders.pop());

    var stats;
    try {
      stats = fs.statSync(homeFolder);
    } catch (err) {
      fs.mkdirSync(homeFolder);
      stats = fs.statSync(homeFolder);
    }

    if (!stats.isDirectory()) {
      throw new Error(homeFolder + 'is not a directory!');
    }

  } 
};

/**
 * Issue a call to the Rumpus daemon to reload its user database
 * 
 * @param {booleanCallback} callback - Response handler (err, boolean)
 */
Server.prototype.reloadUserDatabase = function(callback) {

  try {
    var client;
    if (url.parse(this.reloadUrl).protocol === 'https:') {
      client = https;
    } else {
      client = http;
    }

    client.get(this.reloadUrl, function(res) {
      var reply = '';
      res.on('data', function (chunk) {
        reply += chunk;
      });
      res.on('end', function() {
        if (res.statusCode != 200) {
          callback('Error reloading user database: ' + res.statusMessage, null);
        } else if (reply === 'User Database Reloaded') {
          callback(null, true);
        } else {
          callback('Error reloading user database: ' + reply, null);
        }
      });
    })
      .on('error', function(err) {
        callback(err, null);
      });
  } catch (err) {
    callback(err, null);
  }
};

/**
 * Save the given user to the Rumpus user database.
 * 
 * The function first validates the user, then synchronously reads the
 * user database file, inserts or updates the user record, and writes
 * the file back to the Rumpus configuration folder. 
 * 
 * The I/O is performed synchronously to avoid race conditions that
 * might otherwise occur due to another concurrently running request.
 * 
 * @param {module:rumpus/user~User} user - the User to save
 * @param {booleanCallback} callback - true if a new user record was created
 */
Server.prototype.save = function(user, callback) {
  var self = this;
  
  user.validate(function (err) {
    if (err) return callback(err);

    try {
      var users = self.getUsersSync();

      var idx = users.findIndex(function(u) {
        return u.accountName.toLowerCase() === user.accountName.toLowerCase();
      });

      var createUser = (idx === -1);
      if (createUser) {
        users.push(user);
      } else {
        users[idx] = user;
      }

      users.sort(function (u1, u2) {
        return u1.accountName.toLowerCase().localeCompare(u2.accountName.toLowerCase());
      });

      var data = users.map(function(userObj) {
        return userObj.toCsvRow();
      });

      self.makeHomeFolder(user.homeFolder);

      fs.writeFileSync(self.userDb, data.join('\r'));

      return callback(null, createUser);

    } catch (err) {
      return callback(err);
    }
  });
};    

/**
 * Delete the user with the given accountName from the Rumpus user database.
 * 
 * The function first locates the user, then synchronously reads the
 * user database file, deletes the user record, and writes
 * the file back to the Rumpus configuration folder. 
 * 
 * The I/O is performed synchronously to avoid race conditions that
 * might otherwise occur due to another concurrently running request.
 * 
 * @param {String} accountName - the accountName to look up
 * @param {callback} callback - true if a new user record was created
 */
Server.prototype.delete = function(accountName, callback) {
  var self = this;

  try {
    var users = self.getUsersSync();

    var idx = users.findIndex(function(u) {
      return u.accountName.toLowerCase() === accountName.toLowerCase();
    });

    if (idx === -1) {
      return callback(new Error('User ' + accountName + ' not found'));
    } else {
      var protectedAccounts = config.get('ProtectedAccounts');

      var protectedIdx = protectedAccounts.findIndex(function(u) {
        return u.toLowerCase() === accountName.toLowerCase();
      });

      if (protectedIdx !== -1) {
        return callback(new Error('Deleting ' + accountName + ' is denied'));
      }
    }

    users.splice(idx, 1);

    users.sort(function (u1, u2) {
      return u1.accountName.toLowerCase().localeCompare(u2.accountName.toLowerCase());
    });

    var data = users.map(function(userObj) {
      return userObj.toCsvRow();
    });

    fs.writeFileSync(self.userDb, data.join('\r'));

    return callback(null);

  } catch (err) {
    return callback(err);
  }
};
