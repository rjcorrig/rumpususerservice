var parseString = require('xml2js').parseString;
var xmlbuilder = require('xmlbuilder');

/**
 * Notice module
 * 
 * @module rumpus/notice
 */
module.exports = Notice;

/**
 * A notice initialization Object
 * @typedef {Object} NoticeInitializer
 * 
 * @property {string} [noticeName] Notice Name
 * @property {string} [onePerSession] Y or N to indicate if a single notice should be sent per session
 * @property {string} [mailServer] The mail server to use
 * @property {string} [mailFrom] The e-mail account to send from
 * @property {string} [mailTo] A comma-separated list of recipients
 * @property {string} [mailCC] A comma-separated list of cc: recipients
 * @property {string} [mailSubject] The subject of the e-mail notice
 */

/**
 * Create a Notice
 * 
 * @constructor
 * @param {module:rumpus/notice~NoticeInitializer} [args={}] - Values to initialize 
 */
function Notice(args) {
  args = args || {};

  /**
   * Notice Name
   * @member {string}
   */
  this.noticeName = args.noticeName || '';

  /**
   * Whether a single notice should be sent per upload session
   * @member {string}
   * @default 'N'
   */
  this.onePerSession = args.onePerSession || 'N';

  /**
   * Mail Server
   * @member {string}
   */
  this.mailServer = args.mailServer || '';

  /**
   * Sender e-mail address
   * @member {string}
   */
  this.mailFrom = args.mailFrom || '';

  /**
   * Comma-separated list of recipients
   * @member {string}
   */
  this.mailTo = args.mailTo || '';

  /**
   * Comma-separated list of cc: recipients
   * @member {string}
   */
  this.mailCC = args.mailCC || '';

  /**
   * Subject line of the e-mail notice
   * @member {string}
   */
  this.mailSubject = args.mailSubject || '';
}

/** 
 * Return a new Notice from a row 
 * 
 * @param {string[]} noticeColumns
 * @returns {module:rumpus/notice~Notice}
 * @throws Will throw a 'CSV row must be an e-mail notice' error if noticeColumns[1] != 1
*/
Notice.fromCsvRow = function(noticeColumns) {
  if (noticeColumns[1] != 1) {
    throw new Error('CSV row must be an e-mail notice');
  }
  var notice = new Notice({
    noticeName: noticeColumns[0],
    onePerSession: noticeColumns[2],
    mailServer: noticeColumns[3],
    mailFrom: noticeColumns[4],
    mailTo: noticeColumns[5],
    mailCC: noticeColumns[6],
    mailSubject: noticeColumns[7] 
  });

  return notice;
};

/**
 * Create a notice from an XML as defined in the Web API
 * 
 * @param {string} xml The XML to convert to a notice
 * @param {noticeCallback} callback Error handler function (err, Notice)
 */
Notice.fromXml = function(xml, callback) {
  parseString(xml, { explicitArray: false }, function(err, result) {
    if (err) return callback(err);

    if (!result.notice) return callback('No notice XML was provided');

    return callback(null, new Notice(result.notice));
  });
};


/**
 * Serialize this Notice as XML as defined in the Web API
 * 
 * @returns {string}
 */
Notice.prototype.toXml = function() {
  return xmlbuilder.create({notice: this}).end();
};
