// Global jsdoc declarations

/**
 * A callback function that expects an error object/string on failure
 * or a User object on success
 *
 * @callback userCallback
 * @param {object} err - an error to send into the callback
 * @param {module:rumpus/user~User} user - a User object to send into the callback
 */

/**
 * A callback function that expects an error object/string 
 * or a User array on success
 *
 * @callback userArrayCallback
 * @param {object} err - an error to send into the callback
 * @param {module:rumpus/user~User[]} users - a User array to send into the callback
 */

/**
 * A callback function that expects an error object/string on failure
 * or a Notice object on success
 *
 * @callback noticeCallback
 * @param {object} err - an error to send into the callback
 * @param {module:rumpus/notice~Notice} notice - a Notice object to send into the callback
 */

/**
 * A callback function that expects an error object/string 
 * or a Notice array on success
 *
 * @callback noticeArrayCallback
 * @param {object} err - an error to send into the callback
 * @param {module:rumpus/notice~Notice[]} notices - a Notice array to send into the callback
 */

/**
 * A callback function that expects an error object/string 
 * or a boolean value indicating success
 *
 * @callback booleanCallback
 * @param {object} err - an error to send into the callback
 * @param {boolean} users - a User array to send into the callback
 */

/**
 * A callback function that expects an error object/string
 * 
 * @callback callback
 * @param {object} err - an error to send into the callback
 */
