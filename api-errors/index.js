module.exports = {

  NotFoundError: function (msg) {
    msg = msg || 'Not Found';
    this.name = 'NotFoundError';
    this.message = msg;
    this.status = 404;
    Error.call(this, msg);
    Error.captureStackTrace(this, arguments.callee);
  },
  BadRequestError: function(msg) {
    msg = msg || 'Bad Request';  
    this.name = 'BadRequestError';
    this.message = msg;
    this.status = 400;
    Error.call(this, msg);
    Error.captureStackTrace(this, arguments.callee);
  },
  ForbiddenError: function(msg) {
    msg = msg || 'Forbidden';
    this.name = 'ForbiddenError';
    this.message = msg;
    this.status = 403;
    Error.call(this, msg);
    Error.captureStackTrace(this, arguments.callee);
  }

};