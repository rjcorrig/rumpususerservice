var express = require('express');
var morganLogger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
require('body-parser-xml')(bodyParser);
var passport = require('passport');
var LocalAPIKeyStrategy = require('passport-localapikey-update').Strategy;
var config = require('config');
var { createLogger, format, transports } = require('winston');
var { combine, timestamp, printf } = format;

var app = express();
var winstonLogger = createLogger({
  format: combine(
    timestamp(),
    printf(({ level, message, timestamp }) => {
      return `${timestamp} ${level.toUpperCase()} ${message}`;
    })
  ),
  transports: [
    new transports.Console({
      stderrLevels: ['error']
    })
  ]
});

// eslint-disable-next-line no-unused-vars
morganLogger.token('url', function(req, res) {
  return req.originalUrl.replace(req.query.apikey, '******');
});
app.use(morganLogger('combined'));

app.use(bodyParser.xml({
  xmlParseOptions: { explicitArray: false }
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(passport.initialize());
passport.use(new LocalAPIKeyStrategy(
  function(apikey, done) {
    if (!config.has('Service.apiKey')) {
      return done(new Error('No api key configured'));
    }

    return done(null, apikey === config.get('Service.apiKey'));
  }
));

var routes = require('./routes/index');
var users = require('./routes/users');
var notices = require('./routes/notices');

app.use('/', routes);
app.use('/users', users);
app.use('/notices', notices);
app.use('/doc', express.static('doc'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  // eslint-disable-next-line no-unused-vars
  app.use(function(err, req, res, next) { 
    res.status(err.status || 500);
    winstonLogger.error(err.stack || err);
    res.send(err.stack || err);
  });
}

// production error handler
// no stacktraces leaked to user
// eslint-disable-next-line no-unused-vars
app.use(function(err, req, res, next) { 
  res.status(err.status || 500);
  winstonLogger.error(err.message || err);
  res.send(err.message || err);
});


module.exports = app;
